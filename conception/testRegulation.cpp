#include <stdio.h>
#include "../board_src/control.h"

/*
Compile with: gcc testRegulation.cpp ../board_src/control.cpp
*/

/*
Resistance termique du frigo:
consomation pour 24h = 168kWh / 365
P = 19.178 W
Rt = deltaT / Flux = 15 / 19.178 = 0.78 K/W
puissance froid => 150W
puissance chaud => 100W
vide => 5kg d'eau + 20kg plastique
plein => 30kg d'eau + 20kg plastique
Cp eau = 4.187 kJ/kgK
Cp plastic = 1.5 kJ/kgK
*/

class Fridge {
private:
  // parameters
  double _tempOut; // deg C
  double _heatPower; // W
  double _coolPower; // W
  double _resistor; // K/W
  double _specificHeat; // J/K
  // internal states
  double _temperature;
public:
  Fridge(bool full = true) {
    setParameters(25., 100., 150., 0.7, full ? 155000. : 51000.);
    init(15.);
  }
  void setParameters(double tempOut, double heatPower, double coolPower,
                     double resistor, double specificHeat) {
    this->_tempOut = tempOut;
    this->_heatPower = heatPower;
    this->_coolPower = coolPower;
    this->_resistor = resistor;
    this->_specificHeat = specificHeat;
  }
  double init(double temp) {
    _temperature = temp;
    return _temperature;
  }
  double step(double dt_s, bool coolOn, bool heatOn) {
    double power = (_tempOut - _temperature) / _resistor;
    if (coolOn) power -= _coolPower;
    if (heatOn) power += _heatPower;
    double energy = power * dt_s;
    _temperature += energy / _specificHeat;
    return _temperature;
  }
  double temperature() {return _temperature;}
};

// low pass wih tau = 1s
double lowPass(double e, double dt_s)
{
  static double s = 0.;
  static double ePrev = 0.;
  if (dt_s < 0.) {
    s = e;
  } else {
    s = s + (dt_s/1.) * (ePrev - s);
  }
  ePrev = e;
  return s;
}

int main() {
  float temp = 10.; // deg C
  float dt = 60.; // second
  Fridge fridge(false);

  // init
  regulator.setThresholdParams(0.8f, 2.f);
  regulator.setPidParams(1.f, 0.f, 0.f, 100.f);
  regulator.setTarget(15.f);
  regulator.setMode(Regulation::Controlling, temp);
  regulator.setType(Regulation::Pid, temp);
  fridge.init(temp);
  //lowPass(temp, -1.);

  // start test
  printf("[%lf", temp);
  for (int i=1; i<500; i++) {
    int coolOn, heatOn;
    if (!regulator.control(temp, dt, &coolOn, &heatOn)) {
      printf("\nError with regulation\n");
      return 1;
    }
    temp = fridge.step(dt, coolOn, heatOn);
    //temp = lowPass(pidValue, dt);
    if ( (i%10) == 0)
      printf(",\n%lf", temp);
    else
      printf(", %lf", temp);
  }
  printf("]\n");
  return 0;
}
