"use strict";

/*
TODO:
 - finish new board function
 - fix position of elements on multiple scale and on save/load
 - add more components
 - add label informations on component, name, editable value
 - normalize component size in lib
 - Add some beauty to library and placement area
 - allow comments
 - manage selection
 - add connection capabilities
*/
var cmpPlace = (function() {

//! an array of elements to move on drag
var movingElts = [];
//! the coordinates of mouse on the grabbed element
var grabPos = {x: 0, y: 0};
//! indicate that selected element should be duplicated on first move
var duplicateOnMove = false;
//! the scale of all displayed components
var scale = 0; // zero is not a valid value, it should be scaled on DOM loading

//! list of available components
const libraryComponent = [{name: 'resistor', package: 'resistor'},
                          {name: 'surf_resistor', package: 'smd1206'},
                          {name: 'surf_led', package: 'smd0805'},
                          {name: 'AMS1117', package: 'sot223'},
                          {name: 'AO3400', package: 'sot23'},
                          {name: 'LM358', package: 'dip8'},
                          {name: 'pinHeader2', package: 'pinHeader2'},
                          {name: 'delimiter2', package: 'delimiter2'},
                          {name: 'delimiter5', package: 'delimiter5'},
                          {name: 'ESP-12F', package: 'ESP-12'},
                          {name: 'trim_potar', package: 'trim_potar'},
                          {name: 'surf_button', package: 'surf_button'}];
const libBaseSize = 30.;

//! make mouse move drag element or later the selection from element
function setDragElement(element, event, duplicate) {
  event.preventDefault();
  duplicateOnMove = duplicate;
  movingElts = [element]; // + selection
  grabPos.x = event.clientX - element.offsetLeft + 5;
  grabPos.y = event.clientY - element.offsetTop + 4;
  // enable drag on mouse move
  window.addEventListener("mousemove", elementDrag);
  window.addEventListener("mouseup", closeDragElement);
  window.addEventListener("blur", closeDragElement);
}

//! stop drag on mouse move
function closeDragElement(event) {
  // stop moving elements when mouse button is released:
  // movingElts = []; // should put this again and manage a selection
  duplicateOnMove = false; // if no move has been done
  window.removeEventListener("mousemove", elementDrag);
  window.removeEventListener("mouseup", closeDragElement);
  window.removeEventListener("blur", closeDragElement);
}

//! put the dragged elements on mouse position
function elementDrag(event) {
  event.preventDefault();
  if (duplicateOnMove) {
    let componentPanel = document.getElementById('placementPanel');
    // duplicate the selected elements and set drag to copied elements
    let oldGrabberBR = movingElts[0].getBoundingClientRect();
    let oldRef = {x: oldGrabberBR.left - movingElts[0].offsetLeft,
                  y: oldGrabberBR.top - movingElts[0].offsetTop};
    movingElts = movingElts.map(function(elt) {
      var newElt = elt.cloneNode(true);
      enableDrag(newElt, false);
      newElt.className = newElt.className + " dragElt";
      scaleComponent(newElt, scale);
      componentPanel.appendChild(newElt);
      return newElt;
    });
    // set grabPos relative to new container
    let newGrabberBR = movingElts[0].getBoundingClientRect();
    let newRef = {x: newGrabberBR.left - movingElts[0].offsetLeft,
                  y: newGrabberBR.top - movingElts[0].offsetTop};
    grabPos.x = grabPos.x - oldRef.x + newRef.x;
    grabPos.y = grabPos.y - oldRef.y + newRef.y;
    duplicateOnMove = false;
  }
  // get the raw element position relative to cursor:
  var eltX = event.clientX - grabPos.x;
  var eltY = event.clientY - grabPos.y;
  // can apply constraint on element position (grid, path, ...) here
  // compute translation to apply
  var tx = eltX - movingElts[0].offsetLeft;
  var ty = eltY - movingElts[0].offsetTop;
  // set the element's new position:
  for (let i=0; i<movingElts.length; i++) {
    movingElts[i].style.left = (movingElts[i].offsetLeft + tx) + "px";
    movingElts[i].style.top = (movingElts[i].offsetTop + ty) + "px";
  }
}

//! enable drag on mouse click for element
function enableDrag(element, duplicate) {
    let grabElements = []; //element.getElementsByClassName("eltGrabber");
    let grabElt = grabElements.length ? grabElements[0] : element;
    grabElt.onmousedown = function(e){setDragElement(element, e, duplicate);};
}

function keyDownFunc(event) {
  if (event.code == "KeyR" && movingElts.length > 0) {
    rotateComponent(movingElts[0], 90);
  } else if (event.code == "KeyD" && movingElts.length > 0) {
    movingElts[0].parentNode.removeChild(movingElts[0]);
  }
}

//! create the elements library
function createLibrary() {
  var libraryContainer = document.getElementById('libraryContainer');
  if  (libraryContainer == null)
    return;
  libraryComponent.forEach(function(component) {
    var newElt = createComponent(component);
    libraryContainer.appendChild(newElt);
    enableDrag(newElt, true);
  });
}

//! return the created element of given type
function createComponent(component) {
  if (libraryComponent.indexOf(component) < 0)
    return null;
  var svgElem = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
  svgElem.style.display = "block";
  var useElem = document.createElementNS('http://www.w3.org/2000/svg', 'use');
  useElem.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', "#" + component.package);
  svgElem.appendChild(useElem);
  var cmp = document.createElement('div');
  cmp.className = "component";
  cmp.setAttribute("data-cmpName", component.name);
  var shape = document.getElementById("componentShapeLib").getElementById(component.package);
  var width = shape.getAttribute("width");
  var height = shape.getAttribute("height");
  cmp.setAttribute("data-libWidth", width);
  cmp.setAttribute("data-libHeight", height);
  cmp.appendChild(svgElem);
  var refSize = Math.max(width, height)
  var libScale = libBaseSize * Math.log10(refSize + 1.) / refSize;
  scaleComponent(cmp, libScale);
  return cmp;
}

function componentRotation(componentElt) {
  var svgElem = componentElt.getElementsByTagName("svg")[0];
  var transAttr = svgElem.getAttribute("transform");
  if (transAttr == null)
    return 0;
  return parseInt(transAttr.substring(7, transAttr.length - 1));
}

function rotateComponent(componentElt, deltaRotation) {
  var rotation = (componentRotation(componentElt) + deltaRotation) % 360;
  var svgElem = componentElt.getElementsByTagName("svg")[0];
  svgElem.setAttribute("transform", "rotate(" + rotation + ")");
}

function scaleComponent(componentElt, newScale) {
  let cmpWidth = componentElt.getAttribute("data-libWidth") * newScale;
  let cmpHeight = componentElt.getAttribute("data-libHeight") * newScale;
  var svgElem = componentElt.getElementsByTagName("svg")[0];
  svgElem.setAttribute("width", cmpWidth);
  svgElem.setAttribute("height", cmpHeight);
  var useElem = svgElem.getElementsByTagName("use")[0];
  useElem.setAttribute("width", cmpWidth);
  useElem.setAttribute("height", cmpHeight);
}

function setScale(newScale) {
  if (scale == newScale)
    return;
  var scaleRatio = scale != 0 ? newScale/scale : newScale;
  scale = newScale;
  // don't scale library
  // board pattern
  var boardPattern = document.getElementById("componentShapeLib").getElementById("boardPattern");
  boardPattern.setAttribute("patternTransform", "scale(" + scale + ")")
  // board size
  var board = document.getElementById('placementPanel');
  board.style.width = (254 * board.getAttribute("data-column") * scale) + "px";
  board.style.height = (254 * board.getAttribute("data-row") * scale) + "px";
  // components
  Array.prototype.forEach.call(board.children, function(componentElt) {
    /* className attribut has a different meaning on svg elements, it can't be used here */
    var eltClass = componentElt.getAttribute("class");
    if (eltClass == null || eltClass.indexOf("component") == -1)
      return;
    // scale
    scaleComponent(componentElt, scale);
    // reposition
    componentElt.style.left = (componentElt.offsetLeft * scaleRatio - 5) + "px";
    componentElt.style.top = (componentElt.offsetTop * scaleRatio - 4) + "px";
  });
}

function resetBoard(row, column, clearContent) {
  var panel = document.getElementById('placementPanel');
  // remove all previous content
  if (clearContent) {
    while (panel.firstChild)
      panel.removeChild(panel.lastChild);
    // create background
    var svgElem = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    svgElem.setAttribute("width", "100%");
    svgElem.setAttribute("height", "100%");
    svgElem.style.display = "block";
    var rectElem = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    rectElem.setAttribute("width", "100%");
    rectElem.setAttribute("height", "100%");
    rectElem.setAttribute("fill", "url(#boardPattern)");
    svgElem.appendChild(rectElem);
    panel.appendChild(svgElem);
  }
  // set required size to board
  panel.setAttribute("data-row", row);
  panel.setAttribute("data-column", column);
  panel.style.width = (254 * column * scale) + "px";
  panel.style.height = (254 * row * scale) + "px";
}

function save() {
  var boardElt = document.getElementById('placementPanel');
  var board = {
    'r': parseInt(boardElt.getAttribute("data-row")),
    'c': parseInt(boardElt.getAttribute("data-column")),
    's': parseFloat(scale)
  };
  var components = Array.from(boardElt.children)
    .filter(function(componentElt) {
      // className attribut has a different meaning on svg elements, it can't be used here
      var eltClass = componentElt.getAttribute("class");
      return eltClass != null && eltClass.indexOf("component") > -1;
    })
    .map(function(componentElt) {
      return {
        'id': componentElt.getAttribute("data-cmpName"),
        'x': componentElt.offsetLeft / scale,
        'y': componentElt.offsetTop / scale,
        'r': componentRotation(componentElt)
      };
    });
  return {'board': board, 'cmp': components};
}

function load(data) {
  resetBoard(data.board.r, data.board.c, true);
  var componentPanel = document.getElementById('placementPanel');
  data.cmp.forEach(function(elt) {
    var componant = libraryComponent.find(function(cmp){return cmp.name == elt.id;});
    var newElt = createComponent(componant);
    enableDrag(newElt, false);
    newElt.className = newElt.className + " dragElt";
    componentPanel.appendChild(newElt);
    rotateComponent(newElt, elt.r);
    newElt.style.left = (elt.x - 5) + "px";
    newElt.style.top = (elt.y - 4) + "px";
  });
  scale = 0;
  setScale(data.board.s);
  return true;
}

// load css file
(function() {
  var scriptSrc = (document.currentScript || document.querySelector('script[src*="componentPlacement.js"]'))
                    .getAttribute("src");
  var cssSrc = scriptSrc.replace(new RegExp('\.js$'), '.css');
  document.writeln('<link rel="stylesheet" type="text/css" href="' + cssSrc + '" />');
})()

// create svg component shape library
// "display: none" hide the pattern, "position: absolute" put it outside flow and don't change layout
document.writeln('<svg style="position: absolute;" id="componentShapeLib" width="0" height="0"\
      xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> \
  <defs> \
    <pattern id="boardPattern" patternUnits="userSpaceOnUse" width="254" height="254" \
             viewBox="0 0 2.54 2.54"> \
      <circle cx="1.27" cy="1.27" r="0.7" stroke-width="0.6" class="board_circle" /> \
    </pattern> \
  </defs> \
  <symbol id="dip8" viewBox="-3.81 0 7.62 10" width="762" height="1000" \
          preserveAspectRatio="xMidYMid meet" refX="center" refY="center"> \
    <rect x="-3.24" y="0" width="6.48" height="10" class="cpt_case" /> \
    <rect x="-3.81" y="0.94" width="0.57" height="0.9" class="cpt_leg" /> \
    <rect x="-3.81" y="3.08" width="0.57" height="1.3" class="cpt_leg" /> \
    <rect x="-3.81" y="5.62" width="0.57" height="1.3" class="cpt_leg" /> \
    <rect x="-3.81" y="8.16" width="0.57" height="0.9" class="cpt_leg" /> \
    <rect x="3.24" y="0.94" width="0.57" height="0.9" class="cpt_leg" /> \
    <rect x="3.24" y="3.08" width="0.57" height="1.3" class="cpt_leg" /> \
    <rect x="3.24" y="5.62" width="0.57" height="1.3" class="cpt_leg" /> \
    <rect x="3.24" y="8.16" width="0.57" height="0.9" class="cpt_leg" /> \
    <path d="M-1.07,0 A1.08,1.08 0 0,0 1.07,0" class="cpt_mark" /> \
  </symbol> \
  <symbol id="sot23" viewBox="0 -1.2 2.9 2.4" width="290" height="240" \
          preserveAspectRatio="xMidYMid meet" refX="center" refY="center"> \
    <rect x="0" y="-0.65" width="2.9" height="1.3" class="cpt_case" /> \
    <rect x="0.3" y="0.65" width="0.4" height="0.55" class="cpt_leg" /> \
    <rect x="2.2" y="0.65" width="0.4" height="0.55" class="cpt_leg" /> \
    <rect x="1.25" y="-1.2" width="0.4" height="0.55" class="cpt_leg" /> \
  </symbol> \
  <symbol id="sot223" viewBox="0 0 6.5 7" width="650" height="700" \
          preserveAspectRatio="xMidYMid meet" refX="center" refY="center"> \
    <rect x="0" y="1.75" width="6.5" height="3.5" class="cpt_case" /> \
    <rect x="1.75" y="0" width="3" height="1.75" class="cpt_leg" /> \
    <rect x="0.6" y="5.25" width="0.7" height="1.75" class="cpt_leg" /> \
    <rect x="2.9" y="5.25" width="0.7" height="1.75" class="cpt_leg" /> \
    <rect x="5.2" y="5.25" width="0.7" height="1.75" class="cpt_leg" /> \
  </symbol> \
  <symbol id="smd0805" viewBox="0 0 2 1.3" width="200" height="130" \
          preserveAspectRatio="xMidYMid meet" refX="center" refY="center"> \
    <rect x="0.5" y="0" width="1" height="1.3" class="cpt_case" /> \
    <rect x="0" y="0" width="0.5" height="1.3" class="cpt_leg" /> \
    <rect x="1.5" y="0" width="0.5" height="1.3" class="cpt_leg" /> \
  </symbol> \
  <symbol id="smd1206" viewBox="0 0 3 1.5" width="300" height="150" \
          preserveAspectRatio="xMidYMid meet" refX="center" refY="center"> \
    <rect x="0.75" y="0" width="1.5" height="1.5" class="cpt_case" /> \
    <rect x="0" y="0" width="0.75" height="1.5" class="cpt_leg" /> \
    <rect x="2.25" y="0" width="0.75" height="1.5" class="cpt_leg" /> \
  </symbol> \
  <symbol id="resistor" viewBox="0 0 7.62 2.2" width="762" height="220" \
          preserveAspectRatio="xMidYMid meet" refX="center" refY="center"> \
    <rect x="0.66" y="0" width="6.3" height="2.2" class="cpt_case" rx="0.8" ry="0.8" /> \
    <rect x="0" y="0.85" width="0.66" height="0.5" class="cpt_leg" /> \
    <rect x="6.96" y="0.85" width="0.66" height="0.5" class="cpt_leg" /> \
  </symbol> \
  <symbol id="pinHeader2" viewBox="0 0 2.4 4.94" width="240" height="494" \
          preserveAspectRatio="xMidYMid meet" refX="center" refY="center"> \
    <circle cx="1.2" cy="1.2" r="1.2" class="cpt_case" /> \
    <circle cx="1.2" cy="3.74" r="1.2" class="cpt_case" /> \
    <rect x="0.2" y="1.2" width="2" height="2.54" class="cpt_case" /> \
    <circle cx="1.2" cy="1.2" r="0.4" class="cpt_leg" /> \
    <circle cx="1.2" cy="3.74" r="0.4" class="cpt_leg" /> \
  </symbol> \
  <symbol id="delimiter2" viewBox="0 0 0.3 5.08" width="30" height="508" \
          preserveAspectRatio="xMidYMid meet" refX="center" refY="center"> \
    <rect x="0" y="0" width="0.3" height="5.08" class="cpt_delimiter" /> \
  </symbol> \
  <symbol id="delimiter5" viewBox="0 0 0.3 12.7" width="30" height="1207" \
          preserveAspectRatio="xMidYMid meet" refX="center" refY="center"> \
    <rect x="0" y="0" width="0.3" height="12.7" class="cpt_delimiter" /> \
  </symbol> \
  <symbol id="surf_button" viewBox="0 0 2.5 8" width="250" height="800" \
          preserveAspectRatio="xMidYMid meet" refX="center" refY="center"> \
    <rect x="0" y="1" width="2.5" height="6" class="cpt_case" rx="1" ry="1" /> \
    <ellipse cx="1.25" cy="4" rx="0.7" ry="2" class="cpt_mark" /> \
    <rect x="1" y="0" width="0.5" height="1" class="cpt_leg" /> \
    <rect x="1" y="7" width="0.5" height="1" class="cpt_leg" /> \
  </symbol> \
  <symbol id="trim_potar" viewBox="0 0 4.85 9.5" width="485" height="950" \
          preserveAspectRatio="xMidYMid meet" refX="center" refY="center"> \
    <rect x="0" y="0" width="4.85" height="9.5" class="cpt_case" /> \
    <circle cx="3.5" cy="1.15" r="1.15" class="cpt_mark" /> \
    <rect x="3.2" y="0" width="0.6" height="2.3" class="cpt_leg" /> \
  </symbol> \
  <symbol id="ESP-12" viewBox="0 0 16 24" width="1600" height="2400" \
          preserveAspectRatio="xMidYMid meet" refX="center" refY="center"> \
    <rect x="0" y="0" width="16" height="24" class="cpt_case" /> \
    <rect x="2.5" y="7.5" width="11" height="15" class="cpt_leg" /> \
    <rect x="0" y="22" width="1.5" height="1" class="cpt_leg" /> \
    <rect x="0" y="20" width="1.5" height="1" class="cpt_leg" /> \
    <rect x="0" y="18" width="1.5" height="1" class="cpt_leg" /> \
    <rect x="0" y="16" width="1.5" height="1" class="cpt_leg" /> \
    <rect x="0" y="14" width="1.5" height="1" class="cpt_leg" /> \
    <rect x="0" y="12" width="1.5" height="1" class="cpt_leg" /> \
    <rect x="0" y="10" width="1.5" height="1" class="cpt_leg" /> \
    <rect x="0" y="8" width="1.5" height="1" class="cpt_leg" /> \
    <rect x="14.5" y="22" width="1.5" height="1" class="cpt_leg" /> \
    <rect x="14.5" y="20" width="1.5" height="1" class="cpt_leg" /> \
    <rect x="14.5" y="18" width="1.5" height="1" class="cpt_leg" /> \
    <rect x="14.5" y="16" width="1.5" height="1" class="cpt_leg" /> \
    <rect x="14.5" y="14" width="1.5" height="1" class="cpt_leg" /> \
    <rect x="14.5" y="12" width="1.5" height="1" class="cpt_leg" /> \
    <rect x="14.5" y="10" width="1.5" height="1" class="cpt_leg" /> \
    <rect x="14.5" y="8" width="1.5" height="1" class="cpt_leg" /> \
    <rect x="2.5" y="23.5" width="1" height="0.5" class="cpt_leg" /> \
    <rect x="4.5" y="23.5" width="1" height="0.5" class="cpt_leg" /> \
    <rect x="6.5" y="23.5" width="1" height="0.5" class="cpt_leg" /> \
    <rect x="8.5" y="23.5" width="1" height="0.5" class="cpt_leg" /> \
    <rect x="10.5" y="23.5" width="1" height="0.5" class="cpt_leg" /> \
    <rect x="12.5" y="23.5" width="1" height="0.5" class="cpt_leg" /> \
  </symbol> \
</svg>');

// when page is loaded add drag function to tagged elements and create the library.
document.addEventListener("DOMContentLoaded", function(event) {
  // enable drag on elements of drag type
  var dragElts = document.getElementsByClassName("dragElt");
  Array.prototype.forEach.call(dragElts, function(elt) {enableDrag(elt, false);});
  createLibrary();
  resetBoard(6, 10, true);
  setScale(0.2);
  document.addEventListener("keydown", keyDownFunc);
});

return {
  getScale: function() { return scale;},
  setScale: setScale,
  resetBoard: resetBoard,
  save: save,
  load: load
}
})();
