# Brewing chamber board interface

## WiFi connection

The board connect to WiFi using credential hard codded in main code file. It gets its IP address from DHCP server.

The main HTML page is reachable through server root or at */index.html* url.


## REST Services URLs

All request to the same URL contains the same content in response. **GET** will only fetch the data,
**POST** and **DELETE** will perform an action and send the same data than a **GET** request after the action has been performed.
For a **DELETE** request the response will often be empty.  
**HEAD** method is always available when **GET** is. It will return the same header but without body.

   method  | URL | description |
:---------:| --- | ----------- |
**GET**    | */*, */index.html*       | Send home page
**PUT**    | */*, */index.html*       | Update home page
**GET**    | */status*                | Show complete state
**POST**   | */status*                | Update control parameters
**GET**    | */error*                 | List last error messages
**DELETE** | */error*                 | Clear error message list
**GET**    | */records*               | List record resources
**POST**   | */records*               | Update all record resources
**DELETE** | */records*               | Delete all record resources
**GET**    | */records/{resource_id}* | Show record resource content
**DELETE** | */records/{resource_id}* | Delete record resource


## Data Format

### /, /index.html

To upload a new home page using cURL:
```bash
curl http://frigo --upload-file index.html.gz
```

### /status

response:
```json
{
  "mode": "Controlling",
  "availableModes": ["Sleeping", "Controlling", "HeatOnly", "CoolOnly", "Watching"],
  "type": "Pid",
  "availableTypes": ["Pid", "Threshold"],
  "target": 15.2,
  "temp": 8.4,
  "cooler": false,
  "heater": true,
  "startDate": "2021-07-18T08:52:12",
  "totalBytes": 2048,
  "usedBytes": 1563,
  "lastSave": "2021-09-06T05:15:42",
  "k": 0.2,
  "R0": 10.0,
  "B": 3200.0,
  "Rref": 20.0,
  "samplePeriod": 30,
  "recordPid": true,
  "pidCycle": 600,
  "pidG": 1.0,
  "pidTi": 0.01,
  "pidTd": 0.0,
  "pidLimit": 100.0,
  "heatPeriod": 30.0,
  "heatRatio": 1.0,
  "heatPower": 0.8,
  "error": ["2021-08-18T10:23:56 something went wrong",
            "2021-08-18T10:23:56 another problem occurred"]
}
```

**POST** request has the same format than response. Only writable parameters will be set, they are:  
`mode` `type` `target` `k` `R0` `B` `Rref` `samplePeriod` `recordPid` `pidCycle`
`pidG` `pidTi` `pidTd` `pidLimit` `heatPeriod` `heatRatio` `heatPower`

### /error

response:
```json
{
  "error": [{"time": "2021-08-18T10:23:56Z", "msg": "something went wrong"},
            {"time": "2021-08-18T10:23:56Z", "msg": "another problem occurred"}]
}
```

### /records

response list available resource id:
```json
{
  "temp": ["records/20210823115218_temp", "records/20210823114618_temp"],
  "pid": ["records/20210823114618_pid"],
  "state": ["records/20210823115226_state"],
  "lastSave": "2021-09-06T05:15:42",
  "open": ["records/20210823115218_temp", "records/20210823115226_state"]
}
```

**POST** request
```json
{
  "action": "saveOnFlash"
}
```
The only available action is *saveOnFlash*, it will save on flash files all data stored in RAM.
This request should be done before shutdown or some data will be lost.

### /records/{resource_id}

mime type is `application/octet-stream`
+ **Temperature** with resource id pattern: *{yyyymmddhhmmss}*_temp, contains a list of temperature sampled at regular interval.  
The resource_id time stamp is the time of first point. The period is the delay between points in second.  
The temperature in Celsius degrees is coded in an integer on the scale: $`T = 30/255 * i`$

| period |  t0   |  t1   |  t2   | ... |
|:------:|:-----:|:-----:|:-----:|:---:|
| 8bits  | 8bits | 8bits | 8bits | ... |

+ **PidValues** with resource id pattern: *{yyyymmddhhmmss}*_pid, contains a list of pid values sampled at regular interval.  
The resource_id time stamp is the time of first point. The period is the delay between points in second as for temperature.  
Each PID value is encoded as [half precision floating point](https://en.wikipedia.org/wiki/Half-precision_floating-point_format)

| period |   v0   |   v1   |   v2   | ... |
|:------:|:------:|:------:|:------:|:---:|
| 16bits | 16bits | 16bits | 16bits | ... |

+ **State** with resource id pattern: *{yyyymmddhhmmss}*_state, contains a list of value changed encoded on 32 bits.  
  - **time offset** is the offset in second from resource_id time stamp, it allows 58.5 days.
  - **short id bit** the bit at index 22 indicate if the size of id, *1*: 1 bit, *0*: 5 bits.
  - **id** the id of the parameter, see table below for its meaning.
  - **value** the new value of the parameter.

| change0 | change1 | change2 | ... |
|:-------:|:-------:|:-------:|:---:|
| 32bits  | 32bits  | 32bits  | ... |

The data structure of a change is one of:

| time offset |   1   |  id   | value |
|:-----------:|:-----:| -----:|:-----:|
|   22bits    | 1bits | 1bits | 8bits |

| time offset |   0   |  id   | value |
|:-----------:|:-----:| -----:|:-----:|
|   22bits    | 1bits | 5bits | 4bits |

Changes **id**, parameters not in table are not used:

|id | parameter |
|:-:|:- |
short 0 | target temperature encoded as in temperature file
1       | door state, *0*: close, *not 0*: open
2       | cooler state, *0*: inactive, *not 0*: active
3       | heater state, *0*: inactive, *not 0*: active


## Hardware pins

```css
             UUUUUUU
|------------*******------------ |
|       1    *******  2          |
|       O    *******  O    4   5 |
|       O    *******  O    O   O |
|            *******  O    O   O |
|            *******  O    O   O |
|                          O   O |
|                        3     O |
|                        O     O |
|                        O     O |
|  reset button          O     O |
|--------------------------------|
```

| 1  |  2   |   3                     |    4         | 5
| -- | ---- | ----------------------- | ------------ | -----
|VCC | 3.3V | sensor calibration test | VCC          | reset
|GND | GND  | thermistor              | GND          | spare
|    | TX   | thermistor              | Relay heater | spare
|    | RX   |                         | Relay cooler | spare
|    |      |                         |              | spare
|    |      |                         |              | spare (use with caution)
|    |      |                         |              | spare (use with caution)
|    |      |                         |              | flash (shortcut to ground to flash)
