#!/usr/bin/python3

import os
import gzip
import re
import base64

def process_line(line, out_file, project_path):
    # check for javascript link
    match = re.search('<script\s[^>]*src="([^"]*)"', line, flags=re.IGNORECASE)
    if match is not None:
        if match.group(1) == 'debugData.js':
            print('removed: ' + line.strip())
            return;
        print('replacing: ' + line.strip())
        out_file.write('<script>\n'.encode('utf-8'))
        with open(os.path.join(project_path, 'www', match.group(1)), 'r') as dump_file:
            for l in dump_file:
                out_file.write(l.encode('utf-8'))
        out_file.write('</script>\n'.encode('utf-8'))
        return

    # check for css link
    match = re.search('<link\s[^>]*rel="stylesheet"[^>]*type="([^"]*)"[^>]*href="([^"]*)"', line, flags=re.IGNORECASE)
    if match is not None:
        print('replacing: ' + line.strip())
        out_file.write(('<style type="' + match.group(1) + '">\n').encode('utf-8'))
        with open(os.path.join(project_path, 'www', match.group(2)), 'r') as dump_file:
            for l in dump_file:
                out_file.write(l.encode('utf-8'))
        out_file.write('</style>\n'.encode('utf-8'))
        return

    # check for image link
    match = re.search('<link\s[^>]*rel="icon"[^>]*type="([^"]*)"[^>]*href="([^"]*)"', line, flags=re.IGNORECASE)
    if match is not None:
        print('replacing: ' + line.strip())
        out_file.write(('<link rel="icon" type="' + match.group(1) + '"  href="data:' + match.group(1) + ';base64,').encode('utf-8'))
        with open(os.path.join(project_path, 'www', match.group(2)), 'rb') as dump_file:
            out_file.write(base64.b64encode(dump_file.read()))
        out_file.write('"/>\n'.encode('utf-8'))
        return

    out_file.write(line.encode('utf-8'))


def main():
    project_path = os.path.dirname(os.path.realpath(__file__))
    with open(os.path.join(project_path, 'www', 'index.html'), 'r') as mainSource:
        with gzip.open(os.path.join(project_path, 'board_src', 'data', 'index.html.gz'), 'wb') as out_file:
        #with open(os.path.join(project_path, 'board_src', 'data', 'index_full.html'), 'wb') as out_file:
            for line in mainSource:
                process_line(line, out_file, project_path)
    print('done')

if __name__ == "__main__":
    # execute only if run as a script
    main()
