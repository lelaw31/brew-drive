"use strict";

/*
todo:
- create a smart time axis
- display value on hover (float on curve, boolean on top or bottom)
- add a lighter secondary grid
- add a way to control zoom with fingers
*/

(function() {

var chartData = [];

function pcv(val) { // print curve value
  return val.toPrecision(7);
}

function buildBooleanPathD(data, dx, dy, boolYVal) {
  var d = '';
  var lastY = '0';
  var nextIsMove = true;
  for (let i=0; i<data.x.length; i++) {
    if (data.x[i] == null) {
      nextIsMove = true;
    } else {
      let currentY = data.y[i] ? boolYVal : '0';
      if (nextIsMove) {
        d = d + " M" + pcv(data.x[i]-dx) + " " + currentY;
        nextIsMove = false;
      } else {
        d = d + " L" + pcv(data.x[i]-dx) + " " + lastY;
        d = d + " L" + pcv(data.x[i]-dx) + " " + currentY;
      }
      lastY = currentY;
    }
  }
  return d;
}

function buildNumberPathD(data, dx, dy) {
  var d = '';
  var nextAction = "M";
  for (let i=0; i<data.x.length; i++) {
    if (data.x[i] == null) {
      nextAction = " M";
    } else {
      d = d + nextAction + pcv(data.x[i]-dx) + " " + pcv(-data.y[i]+dy);
      nextAction = " L";
    }
  }
  return d;
}

function buildPath(data, dx, dy, clipId, boolYVal) {
  var svgPath = document.createElementNS("http://www.w3.org/2000/svg", "path");
  svgPath.setAttribute('clip-path', 'url(#' + clipId + ')');
  svgPath.setAttribute('vector-effect', 'non-scaling-stroke');
  if (data.hasOwnProperty('cssClass'))
    svgPath.setAttribute('class', data.cssClass);
  if (typeof data.y[0] == "boolean")
    svgPath.setAttribute('d', buildBooleanPathD(data, dx, dy, boolYVal));
  else
    svgPath.setAttribute('d', buildNumberPathD(data, dx, dy));
  return svgPath;
}

function buildGrid(xAxis, yAxis, clipId) {
  var g = document.createElementNS("http://www.w3.org/2000/svg", "g");
  g.setAttribute('class', 'ChartGrid');
  for (let i=0; i<xAxis.steps.length; i++) {
    let vLine = document.createElementNS("http://www.w3.org/2000/svg", "line");
    vLine.setAttribute('clip-path', 'url(#' + clipId + ')');
    vLine.setAttribute('vector-effect', 'non-scaling-stroke');
    vLine.setAttribute('x1', pcv(xAxis.steps[i].val));
    vLine.setAttribute('y1', pcv(0));
    vLine.setAttribute('x2', pcv(xAxis.steps[i].val));
    vLine.setAttribute('y2', pcv(-yAxis.displayLength));
    g.appendChild(vLine);
  }
  for (let i=0; i<yAxis.steps.length; i++) {
    let hLine = document.createElementNS("http://www.w3.org/2000/svg", "line");
    hLine.setAttribute('clip-path', 'url(#' + clipId + ')');
    hLine.setAttribute('vector-effect', 'non-scaling-stroke');
    hLine.setAttribute('x1', pcv(0));
    hLine.setAttribute('y1', pcv(-yAxis.steps[i].val));
    hLine.setAttribute('x2', pcv(xAxis.displayLength));
    hLine.setAttribute('y2', pcv(-yAxis.steps[i].val));
    g.appendChild(hLine);
  }
  return g;
}

function buildGraph(curveIndex, xAxisData, yAxisData) {
  const width = pcv(xAxisData.displayLength);
  const height = pcv(yAxisData.displayLength);
  const clipId = 'viewBoxClip' + curveIndex;
  var svgNode = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  svgNode.setAttribute('viewBox', '0 ' + -height + ' ' + width + ' ' + height);
  svgNode.setAttribute('preserveAspectRatio', 'none');
  var clip = document.createElementNS("http://www.w3.org/2000/svg", "clipPath");
  clip.setAttribute('id', clipId);
  var clipRect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
  clipRect.setAttribute('x', 0);
  clipRect.setAttribute('y', -height);
  clipRect.setAttribute('width', width);
  clipRect.setAttribute('height', height);
  clip.appendChild(clipRect);
  svgNode.appendChild(clip);
  svgNode.appendChild(buildGrid(xAxisData, yAxisData, clipId));
  const data = chartData[curveIndex];
  var boolYValFactor = -0.85
  for (let i=0; i<data.curves.length; i++) {
    svgNode.appendChild(buildPath(data.curves[i], xAxisData.startVal, yAxisData.startVal,
                                  clipId, pcv(yAxisData.displayLength*boolYValFactor)));
    if (typeof data.curves[i].y[0] == "boolean")
      boolYValFactor = boolYValFactor + 0.01;
  }
  return svgNode;
}

function labelFormat(val) {
  return val.toLocaleString(undefined, {maximumSignificantDigits: 4});
}

function breakAxisEvenly(startVal, displayLength, minTicks, labelFunc, validSteps) {
  // compute steps
  const stepVal = displayLength / minTicks;
  const finalStep = validSteps.reduce(function(acc, val){return val<stepVal ? val : acc;});
  // get relevant first value
  var axisVal = Math.floor(startVal/finalStep) * finalStep;
  while (axisVal < startVal) axisVal += finalStep;
  axisVal = axisVal - startVal;
  // build data
  var axisData = {steps: [], startVal: startVal, displayLength: displayLength, context: ''};
  while (axisVal < displayLength) {
    axisData.steps.push({val: axisVal, disp: labelFunc(startVal + axisVal)});
    axisVal += finalStep;
  }
  return axisData;
}

function breakAxisOnMonth(startVal, displayLength, minTicks, labelFunc)
{
  return breakAxisEvenly(startVal, displayLength, minTicks, labelFunc,
                         [1, 2, 3, 6].map(function(x){return x*2678400;}));
}

function breakAxisOnYear(startVal, displayLength, minTicks, labelFunc)
{
  return breakAxisEvenly(startVal, displayLength, minTicks, labelFunc,
                         [1, 2, 5, 10].map(function(x){return x*31622400;}));
}

/* always create an svg starting at 0,0 to avoid precision problems */
function computeAxis(startVal, displayLength, minTicks) {
  const stepVal = displayLength / minTicks;
  const scaleFactor = Math.pow(10, Math.floor(Math.log(stepVal)/Math.log(10)));
  const validSteps = [scaleFactor, 2*scaleFactor, 5*scaleFactor];
  return breakAxisEvenly(startVal, displayLength, minTicks, labelFormat, validSteps);
}

function computeTimeAxis(startVal, displayLength, minTicks)
{
  // check relevant axis base (second, minute, hour, day, month, year)
  // these functions use Date oject to convert UTC time to local
  function printSecond(val) {
    const sec = new Date(val*1000).getSeconds();
    return (sec == 0) ? printMinute(val) + ":00" : labelFormat(sec);
  }
  function printMinute(val) {
    const min = new Date(val*1000).getMinutes();
    return (min == 0) ? printHour(val) + ":00" : min;
  }
  function printHour(val) {
    const hour = new Date(val*1000).getHours();
    return (hour == 0) ? printday(val) + ":00" : hour;
  }
  function printDay(val) {
    const day = new Date(val*1000).getDate();
    return (day == 1) ? '1 ' + printMonth(val) : day;
  }
  function printMonth(val) {
    const month = new Date(val*1000).toDateString().substr(4, 3);// "Wed Jul 28"
    return (month == 1) ? printYear(val) : month;
  }
  function printYear(val) {
    return new Date(val*1000).getFullYear();
  }
  var axisData = null;
  const refDateStr = new Date(startVal*1000).toString();
  const timeZoneStr = refDateStr.substr(24, 9);
  const stepVal = displayLength / minTicks;
  if (stepVal < 60) { // second
    axisData = breakAxisEvenly(startVal, displayLength, minTicks, printSecond,
                               [1, 2, 5, 10, 20, 30]);
    axisData.context = refDateStr.substr(0, 21) + timeZoneStr;
  } else if (stepVal < 3600) {
    axisData = breakAxisEvenly(startVal, displayLength, minTicks, printMinute,
                               [1, 2, 5, 10, 20, 30].map(function(x){return x*60;}));
    axisData.context = refDateStr.substr(0, 18) + 'h' + timeZoneStr;
  } else if (stepVal < 86400) {
    axisData = breakAxisEvenly(startVal, displayLength, minTicks, printHour,
                               [1, 2, 4, 8, 12].map(function(x){return x*3600;}));
    axisData.context = refDateStr.substr(0, 15) + timeZoneStr;
  } else if (stepVal < 2678400) {
    axisData = breakAxisEvenly(startVal, displayLength, minTicks, printDay,
                               [1, 2, 5, 10].map(function(x){return x*86400;}));
    axisData.context = refDateStr.substr(4, 3) + refDateStr.substr(10, 5) + timeZoneStr;
  } else if (stepVal < 31622400) {
    axisData = breakAxisOnMonth(startVal, displayLength, minTicks, printMonth);
    axisData.context = refDateStr.substr(11, 4) + timeZoneStr;
  } else {
    axisData = breakAxisOnYear(startVal, displayLength, minTicks, printYear);
  }
  return axisData;
}

function buildAxisDom(orientation, axisData) {
  var axis = document.createElement("div");
  axis.className = orientation + '-axis';
  for (let i=0; i<axisData.steps.length; i++) {
    let label = document.createElement("span");
    label.appendChild(document.createTextNode(axisData.steps[i].disp));
    if (orientation == "x")
      label.style.left = ((axisData.steps[i].val / axisData.displayLength)*100 - 5) + '%';
    else
      label.style.bottom = (axisData.steps[i].val /axisData.displayLength)*100 + '%';
    axis.appendChild(label);
  }
  if (orientation == "x") {
    let absLabel = document.createElement("div");
    absLabel.appendChild(document.createTextNode(axisData.context));
    axis.appendChild(absLabel);
  }
  return axis;
}

var rb = {elt: null, clickX: 0, clickY: 0, chart: null, svg: null};

function rubberBandOpen(chartElt, evt) {
  if (evt.button != 0)
    return;
  // avoid selection
  evt.preventDefault();
  // init rubberBand data
  rb.chart = chartElt;
  rb.elt = chartElt.getElementsByClassName("rubberBand")[0];
  rb.svg = chartElt.getElementsByTagName("svg")[0];
  const rect = rb.svg.getBoundingClientRect();
  rb.clickX = evt.clientX - rect.left;
  rb.clickY = evt.clientY - rect.top;
  // set rubberBanding listeners
  window.addEventListener("mousemove", rubberBandResize);
  window.addEventListener("mouseup", rubberBandApply);
  window.addEventListener("blur", rubberBandClose);
  // init rubberBand element
  rb.elt.style.left = rb.clickX + "px";
  rb.elt.style.top = rb.clickY + "px";
  rb.elt.style.width = "1px";
  rb.elt.style.height = "1px";
  rb.elt.style.display = 'block';
}
function rubberBandClose(evt) {
  window.removeEventListener("mousemove", rubberBandResize);
  window.removeEventListener("mouseup", rubberBandApply);
  window.removeEventListener("blur", rubberBandClose);
  rb.elt.style.display = 'none';
}

function rubberBandApply(evt) {
  if (rb.elt.offsetWidth < 6 && rb.elt.offsetHeight < 6) {
    rubberBandClose(evt);
    return;
  }
  const viewBox = JSON.parse(rb.chart.getAttribute('data-viewBox'));
  const chartWidth = rb.chart.clientWidth;
  const chartHeight = rb.chart.clientHeight;
  const bottomLeft = {x: rb.elt.offsetLeft, y: rb.elt.offsetTop+rb.elt.offsetHeight};
  const topRight = {x: rb.elt.offsetLeft+rb.elt.offsetWidth, y: rb.elt.offsetTop};
  rubberBandClose(evt);
  const bl = computeCurveCoordinates(bottomLeft, chartWidth, chartHeight, viewBox);
  const tr = computeCurveCoordinates(topRight, chartWidth, chartHeight, viewBox);
  renderChart(rb.chart, {x: bl.x, width: tr.x-bl.x, y: bl.y, height: tr.y-bl.y});
}

function rubberBandResize(evt) {
  const rect = rb.svg.getBoundingClientRect();
  const mouseX = evt.clientX - rect.left;
  const mouseY = evt.clientY - rect.top;
  if (mouseX > rb.clickX) {
    rb.elt.style.left = rb.clickX + "px";
    rb.elt.style.width = (mouseX - rb.clickX) + "px";
  } else {
    rb.elt.style.left = mouseX + "px";
    rb.elt.style.width = (rb.clickX - mouseX) + "px";
  }
  if (mouseY > rb.clickY) {
    rb.elt.style.top = rb.clickY + "px";
    rb.elt.style.height = (mouseY - rb.clickY) + "px";
  } else {
    rb.elt.style.top = mouseY + "px";
    rb.elt.style.height = (rb.clickY - mouseY) + "px";
  }
}

function computeCurveCoordinates(pt, width, height, viewBox) {
  return {x: (pt.x/width) * viewBox.width + viewBox.x,
          y:((height-pt.y) / height) * viewBox.height + viewBox.y};
}

function renderChart(chartElt, viewBox) {
  // clear old chart content
  while (chartElt.firstChild)
    chartElt.removeChild(chartElt.lastChild);
  // compute axis
  const xAxisData = computeTimeAxis(viewBox.x, viewBox.width, 5);
  const yAxisData = computeAxis(viewBox.y, viewBox.height, 3);
  chartElt.setAttribute('data-viewBox', JSON.stringify(viewBox));
  var svgNode = buildGraph(chartElt.getAttribute("data-curveIndex"), xAxisData, yAxisData);
  chartElt.appendChild(svgNode);
  chartElt.appendChild(buildAxisDom("x", xAxisData));
  chartElt.appendChild(buildAxisDom("y", yAxisData));
  var rubberBand = document.createElement("div");
  rubberBand.setAttribute('class', 'rubberBand');
  chartElt.appendChild(rubberBand);
  chartElt.onmousedown = function(evt) { rubberBandOpen(chartElt, evt); };
  chartElt.ondblclick = function(evt) {
    const defaultViewBox = JSON.parse(rb.chart.getAttribute('data-defaultViewBox'));
    renderChart(chartElt, defaultViewBox);
  }
}

function createLegend(chartElt, curves) {
  var legendElt = document.getElementById(chartElt.getAttribute("data-legendId"));
  if (legendElt == null)
    return;
  for (let i=0; i<curves.length; i++) {
    var legendLine = document.createElement("div");
    var svgNode = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    if (curves[i].hasOwnProperty('cssClass'))
      svgNode.setAttribute('class', curves[i].cssClass);
    let line = document.createElementNS("http://www.w3.org/2000/svg", "line");
    line.setAttribute('x1', '2');
    line.setAttribute('y1', '10');
    line.setAttribute('x2', '23');
    line.setAttribute('y2', '10');
    svgNode.appendChild(line);
    legendLine.appendChild(svgNode);
    legendLine.appendChild(document.createTextNode(curves[i].label));
    legendElt.appendChild(legendLine);
  }
  var spacer = document.createElement('div');
  spacer.setAttribute('class', 'spacer');
  legendElt.appendChild(spacer);
  var updateButton = document.createElement('button');
  updateButton.appendChild(document.createTextNode('Update'));
  updateButton.onclick = function(evt) {loadData(chartElt);};
  legendElt.appendChild(updateButton);
}

function adjustViewBox(min, max) {
  if (min == Infinity) min = 0.;
  if (max == Infinity) max = 1.;
  if (min == max) {
    if (min == 0) {
      min = -1.;
      max = 1.;
    } else {
      min = min * 0.9;
      max = max * 1.1;
    }
  }
  return {min:min, max:max};
}

function checkCurvesData(data) {
  var validatedData = {curves: []};
  var errorMsg = '';
    // compute minimal viewBox to have the complete view
  var minX = Infinity, maxX = -Infinity, minY = Infinity, maxY = -Infinity;
  for (let j=0; j<data.curves.length; j++) {
    let curve = data.curves[j];
    if (curve.x.length != curve.y.length) {
      errorMsg = errorMsg + 'bad data for curve "' + curve.label
                 + '": x and y length missmatch\n';
      continue;
    }
    let lastX = -Infinity;
    for (let i=0; i<curve.x.length; i++) {
      if (curve.x[i] === null)
        continue;
      if (curve.x[i] <= lastX) {
        errorMsg = errorMsg + 'bad data for curve "' + curve.label
                   + '": x values are not strictly increasing\n';
        lastX = -Infinity;
        break;
      }
      lastX = curve.x[i];
      if (curve.x[i] < minX) minX = curve.x[i];
      if (curve.x[i] > maxX) maxX = curve.x[i];
      if (typeof curve.y[i] != "boolean") {
        if (curve.y[i] < minY) minY = curve.y[i];
        if (curve.y[i] > maxY) maxY = curve.y[i];
      }
    }
    if (lastX != -Infinity)
      validatedData.curves.push(curve);
  }
  const rangeX = adjustViewBox(minX, maxX);
  const rangeY = adjustViewBox(minY, maxY);
  const viewBox = {x: rangeX.min, width: rangeX.max-rangeX.min,
                   y: rangeY.min, height: rangeY.max-rangeY.min};
  return {validatedData: validatedData, viewBox: viewBox, errorMsg: errorMsg};
}

function displayErrorMessage(chartElt, errorMsg) {
  while (chartElt.firstChild)
    chartElt.removeChild(chartElt.lastChild);
  var errorBox = document.createElement("div");
  errorBox.setAttribute('class', 'lineChart_error');
  errorBox.appendChild(document.createTextNode(errorMsg));
  chartElt.appendChild(errorBox);
}

function loadData(chartElt) {
  // clear old chart and legend content
  while (chartElt.firstChild)
    chartElt.removeChild(chartElt.lastChild);
  var legendElt = document.getElementById(chartElt.getAttribute("data-legendId"));
  while (legendElt.firstChild)
    legendElt.removeChild(legendElt.lastChild);
  // put waiting element
  var loader = document.createElement("div");
  loader.setAttribute('class', 'loader');
  chartElt.appendChild(loader);
  // update the data
  var namespaces = chartElt.getAttribute("data-curveFunc").split(".");
  var curveFunc = window;
  for(var i = 0; i < namespaces.length; i++)
    curveFunc = curveFunc[namespaces[i]];
  curveFunc(function(data) {
    var res = checkCurvesData(data);
    if (res.validatedData.curves.length == 0) {
      displayErrorMessage(chartElt, res.errorMsg ? res.errorMsg : "No data available")
    } else {
      chartData[chartElt.getAttribute("data-curveIndex")] = res.validatedData;
      chartElt.setAttribute('data-defaultViewBox', JSON.stringify(res.viewBox));
      renderChart(chartElt, res.viewBox);
      if (res.errorMsg.length > 0)
        alert(res.errorMsg);
    }
    createLegend(chartElt, res.validatedData.curves);
  },
  function(errorMsg) {
    displayErrorMessage(chartElt, errorMsg);
    createLegend(chartElt, [])
  });
}

document.addEventListener("DOMContentLoaded", function () {
  var lineCharts = document.getElementsByClassName("LineChart");
  for (let i=0; i<lineCharts.length; i++) {
    chartData.push({});
    lineCharts[i].setAttribute("data-curveIndex", i);
    loadData(lineCharts[i]);
  }
});

})();
