"use strict";

/*
todo:
- set good curve color on graph
- add function to remove only some files, the oldest
- add a waiting symbol when a long operation is in progress
- put an autorefresh
- create progress bar for graph data fetching
- test touch control, mainly on graph.
- put everything in one file and zip it for delivery
- improve visibility of editable values.
- improve parameter part, add algorithm description
*/

var brewChamber = (function() {

const srmCodes = ["#FFE699", "#FFD878", "#FFCA5A", "#FFBF42", "#FBB123", "#F8A600",
                 "#F39C00", "#EA8F00", "#E58500", "#DE7C00", "#D77200", "#CF6900",
                 "#CB6200", "#C35900", "#945534", "#925536", "#844C33", "#744229",
                 "#683A22", "#573512", "#492A0B", "#3A2102", "#361F1B", "#261716",
                 "#231716", "#19100F", "#16100F", "#120D0C", "#100B0A", "#050B0A"];
// key: is the same key than json from board
// label: is the displayed label when editing
// id: is the id of dom element that display the parameter value
const paramNames = {mode: {label: 'Mode', id: 'st_mode', type: 'string'},
                    type:  {label: 'Regulation type', id: 'st_type', type: 'string'},
                    target: {label: 'Target temperature', id: 'st_target', type: 'float'},
                    k: {label: 'k', id: 'temp_k', type: 'float'},
                    R0: {label: 'R0', id: 'temp_R0', type: 'float'},
                    B: {label: 'B', id: 'temp_B', type: 'float'},
                    Rref: {label: 'Rref', id: 'temp_Rref', type: 'float'},
                    samplePeriod: {label: 'Sampling period', id: 'param_sampling', type: 'int'},
                    recordPid: {label: 'Record PID values', id: 'st_recordPid', type: 'bool'},
                    pidCycle: {label: 'pid cycle length', id: 'pid_cycle', type: 'int'},
                    pidG: {label: 'pid G', id: 'pid_g', type: 'float'},
                    pidTi: {label: 'pid Ti', id: 'pid_ti', type: 'float'},
                    pidTd: {label: 'pid Td', id: 'pid_td', type: 'float'},
                    pidLimit: {label: 'pid limit', id: 'pid_limit', type: 'float'},
                    heatPeriod: {label: 'Heater base duration', id: 'heat_period', type: 'float'},
                    heatRatio: {label: 'Heater "PWM" ratio (0 to 1)', id: 'heat_ratio', type: 'float'},
                    heatPower: {label: 'Heater power ratio from cooler', id: 'heat_power', type: 'float'}};
const fsColors = {free: '#339944', used: '#662222'};
var state = null;
var pendingChanges = {};
var graph_data = null;
var fetchCount = 0;

function setSRM(srm) {
  if (srm > 16) {
    var color = 'white';
    var aHoverColor = '#103791';
    var shadowColor = 'rgba(255,255,255,0.3)';
  } else {
    var color = 'black';
    var aHoverColor = '#6986CB';
    var shadowColor = 'rgba(0,0,0,0.3)';
  }
  var css = 'nav, nav a , button, #hist_legend {background-color: ' + srmCodes[srm-1] + '; color: ' + color + ';}\n'
          + '#changePanel {border-color: ' + srmCodes[srm-1] + ';}\n'
          + '.loader { border-top-color: ' + srmCodes[srm-1] + ';}\n'
          + '.logo_base, .logo_decorate {stroke: ' + color + ';}\n'
          + 'button:hover, button:focus, nav a:hover, nav a:active {background-color: ' + aHoverColor + ';}\n'
          + '#hist_legend button {box-shadow: inset 0 0 0.2em 0 ' + shadowColor + ';}\n'
          + '.editable {text-shadow: 0 0 0.8em ' + srmCodes[srm-1] + ';}';
  var style = document.getElementById('srmStyle');
  style.textContent = css;
  // set button text
  document.getElementById('srm_button').textContent = "SRM " + srm;
}

function addClass(elt, className) {
  const currentClass = elt.getAttribute('class');
  if (!currentClass.match(new RegExp("\\b" + className + "\\b")))
    elt.setAttribute('class', currentClass + " " + className);
}
function removeClass(elt, className) {
  const currentClass = elt.getAttribute('class');
  const regexp = new RegExp("\\b" + className + "\\b", "g");
  elt.setAttribute('class', currentClass.replace(regexp, '').replace(/\s+/g, ' '));
}

function decodeFloat16(binary) {
    var exponent = (binary & 0x7C00) >> 10,
        fraction = binary & 0x03FF;
    return (binary >> 15 ? -1 : 1) * (
        exponent ?
        (
            exponent === 0x1F ?
            fraction ? NaN : Infinity :
            Math.pow(2, exponent - 15) * (1 + fraction / 0x400)
        ) :
        6.103515625e-5 * (fraction / 0x400)
    );
};

function displayError(message) {
  displayMessage(message, 'error');
}

function displayMessage(message, type) {
  var messageElt = document.getElementById('messageDisp');
  messageElt.textContent = message;
  messageElt.setAttribute('class', 'msg_' + type);
  messageElt.style.opacity = '0';
  messageElt.style.transition = 'none';
  messageElt.style.display = 'inline-block';
  // need a delay to make render understand the transition after display change
  window.setTimeout(function() {
    messageElt.style.opacity = '1';
    messageElt.style.transition = 'opacity 0.5s linear';
  }, 200);
}

function start_fetch() {
  if (fetchCount == 0) {
    var waitBar = document.getElementById('wait_bar');
    waitBar.style.display = 'block';
    waitBar.setAttribute('class', 'waitbar_inf');
  }
  fetchCount = fetchCount + 1;
}

function stop_fetch() {
  if (fetchCount < 2) {
    var waitBar = document.getElementById('wait_bar');
    waitBar.style.display = 'none';
    waitBar.setAttribute('class', '');
    fetchCount = 0;
  } else {
    fetchCount = fetchCount - 1;
  }
}

if (typeof stubSendHTTP == 'undefined') {
  var sendHTTP = function(method, url, content, errorMsg, successFunc, errorFunc) {
    if (typeof errorFunc === 'undefined')
      errorFunc = function(errMsg) {stop_fetch(); displayError(errMsg);};
    var xhr = new XMLHttpRequest();
    xhr.open(method, url + '?' + new Date().getTime(), true);
    xhr.responseType = (url.substring(0, 'records/'.length) == 'records/') ? 'arraybuffer' : 'json';
    xhr.onload = function() {
      if (xhr.status === 200 || xhr.status == 204) {
        successFunc(xhr.response);
      } else {
        errorFunc(errorMsg + ': ' + xhr.response);
      }
    };
    xhr.onerror = function() {
      errorFunc(errorMsg);
    };
    if (method == 'POST')
      xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(content);
  }
} else {
  var sendHTTP = stubSendHTTP;
}

function fetchNextresource() {
  function fetchOneResource(fileList, recordFunc) {
    sendHTTP('GET', fileList[0], undefined,
             'Error from server to fetch ' + fileList[0], recordFunc,
             function (errMsg) {
               displayError(errMsg);
               fileList.shift();
               fetchNextresource()
             });
  }
  if (graph_data.resources.temp.length > 0) {
    fetchOneResource(graph_data.resources.temp, recordGraphTemp);
  } else if (graph_data.resources.pid.length > 0) {
    fetchOneResource(graph_data.resources.pid, recordGraphPid);
  } else if (graph_data.resources.state.length > 0) {
    fetchOneResource(graph_data.resources.state, recordGraphState);
  } else {
    graph_data.successFunc({curves: [graph_data.temp, graph_data.pid, graph_data.target,
                                     graph_data.door, graph_data.cooler, graph_data.heater]});
    graph_data = null;
    stop_fetch();
  }
}

function resourceStartTime(resource) {
  const date = new Date(Date.UTC(resource.substr(8, 4), resource.substr(12, 2), resource.substr(14, 2),
                                 resource.substr(16, 2), resource.substr(18, 2), resource.substr(20, 2)));
  return Math.floor(date.getTime() / 1000);
}

function checkRecordBreak(curve, startTime, period) {
  if (curve.x.length == 0)
    return;
  if (startTime - curve.x[curve.x.length-1] > period * 2.) {
    curve.x.push(null);
    curve.y.push(null);
  }
}

function recordGraphTemp(data) {
  const resource = graph_data.resources.temp.shift();
  if (data.byteLength > 1) {
    const startTime = resourceStartTime(resource);
    var arr = new Uint8Array(data);
    const period = arr[0];
    checkRecordBreak(graph_data.temp, startTime, period);
    for (let i=1; i<arr.length; i++) {
      graph_data.temp.x.push(startTime + period*(i-1));
      graph_data.temp.y.push((arr[i]/255.) * 30.);
    }
  }
  fetchNextresource();
}

function recordGraphPid(data) {
  const resource = graph_data.resources.pid.shift();
  if (data.byteLength > 1) {
    const startTime = resourceStartTime(resource);
    var arr = new Uint16Array(data);
    const period = arr[0];
    checkRecordBreak(graph_data.pid, startTime, period);
    for (let i=1; i<arr.length; i++) {
      graph_data.pid.x.push(startTime + period*(i-1));
      graph_data.pid.y.push(decodeFloat16(arr[i]));
    }
  }
  fetchNextresource();
}

function recordGraphState(data) {
  const resource = graph_data.resources.state.shift();
  const startTime = resourceStartTime(resource);
  checkRecordBreak(graph_data.door, startTime, 60.);
  checkRecordBreak(graph_data.cooler, startTime, 60.);
  checkRecordBreak(graph_data.heater, startTime, 60.);
  checkRecordBreak(graph_data.target, startTime, 60.);
  var arr = new Uint32Array(data);
  var curve = null;
  for (let i=0; i<arr.length; i++) {
    let cellData = arr[i];
    let eventId = (cellData >> 4) & 0x1f;
    if (cellData & 0x200)
      eventId = cellData & 0x300;
    if (eventId == 1)
      curve = graph_data.door;
    else if (eventId == 2)
      curve = graph_data.cooler;
    else if (eventId == 3)
      curve = graph_data.heater;
    else if (eventId == 0x200)
      curve = graph_data.target;
    else {
      displayError('While analyzing resource "' + resource + '": unknown event id ' + eventId);
      continue;
    }
    curve.x.push(startTime + (cellData >> 10));
    if (curve == graph_data.target)
      curve.y.push(((cellData & 0xff)/255.) * 30.);
    else
      curve.y.push((cellData & 0xf) != 0);
  }
  fetchNextresource();
}

function fetchData(successFunc, errorFunc) {
  if (graph_data != null) {
    errorFunc('Another data fetching operation is already in progress');
    return;
  }
  graph_data = {
    resources: {temp: [], pid: [], state: []},
    temp: {x: [], y: [], label: 'temp', unit: 'C', cssClass: 'curve_temp'},
    pid: {x: [], y: [], label: 'pid', cssClass: 'curve_pid'},
    target: {x: [], y: [], label: 'target', unit: 'C', cssClass: 'curve_target'},
    door: {x: [], y: [], label: 'door', cssClass: 'curve_door'},
    cooler: {x: [], y: [], label: 'cooler', cssClass: 'curve_cooler'},
    heater: {x: [], y: [], label: 'heater', cssClass: 'curve_heater'},
    successFunc: successFunc,
    errorFunc: errorFunc
  }
  start_fetch();
  sendHTTP('GET', 'records', undefined, 'Error from server to fetch records', function(data) {
    if ('temp' in data)
      graph_data.resources.temp = data.temp.sort();
    if ('pid' in data)
      graph_data.resources.pid = data.pid.sort();
    if ('state' in data)
      graph_data.resources.state = data.state.sort();
    fetchNextresource();
  }, function(errMsg) { stop_fetch(); errorFunc(errMsg);});
};

function updateData(data) {
  state = data;
  document.getElementById('st_mode').textContent = data.mode;
  var st_type = document.getElementById('st_type');
  if (data.mode == "Controlling" || data.mode == "CoolOnly" || data.mode == "HeatOnly")
    removeClass(st_type, "faded");
  else
    addClass(st_type, "faded");
  st_type.textContent = data.type;
  if (data.temp < -274) // this is cold !
    document.getElementById('st_temp').textContent = "-\u2103";
  else
    document.getElementById('st_temp').textContent = data.temp.toFixed(1) + "\u2103";
  document.getElementById('st_target').textContent = data.target.toFixed(1);
  document.getElementById('st_cooler').setAttribute("class", data.cooler ? "sign_val sign_on" : "sign_val sign_off");
  document.getElementById('st_heater').setAttribute("class", data.heater ? "sign_val sign_on" : "sign_val sign_off");
  document.getElementById('ft_start').textContent = new Date(data.startDate).toLocaleString("fr-FR");
  var fsUsage = document.getElementById('st_fsUsage');
  var formatFsSize = function(val) {return val + 'o';};
  if (data.totalBytes > 10000)
    formatFsSize = function(val) {return (val/1024.).toFixed(0) + 'ko';};
  fsUsage.textContent = formatFsSize(data.usedBytes) + ' / ' + formatFsSize(data.totalBytes);
  var breakPoint = (data.usedBytes / data.totalBytes) * fsUsage.clientWidth; // position in pixel
  fsUsage.style.background = "linear-gradient(90deg, " + fsColors.used + " " + breakPoint + "px, "
      + fsColors.free + " " + breakPoint + "px, " + fsColors.free + ")";
  document.getElementById('st_recordPid').textContent = data.recordPid.toString();
  document.getElementById('st_lsLastSave').textContent = new Date(data.lastSave).toLocaleString("fr-FR");
  document.getElementById('temp_k').textContent = data.k;
  document.getElementById('temp_R0').textContent = data.R0;
  document.getElementById('temp_B').textContent = data.B;
  document.getElementById('temp_Rref').textContent = data.Rref;
  document.getElementById('param_sampling').textContent = data.samplePeriod;
  document.getElementById('pid_g').textContent = data.pidG;
  document.getElementById('pid_ti').textContent = data.pidTi;
  document.getElementById('pid_td').textContent = data.pidTd;
  document.getElementById('pid_limit').textContent = data.pidLimit;
  document.getElementById('pid_cycle').textContent = data.pidCycle;
  document.getElementById('heat_period').textContent = data.heatPeriod;
  document.getElementById('heat_ratio').textContent = data.heatRatio;
  document.getElementById('heat_power').textContent = data.heatPower;
  document.getElementById('ft_update').textContent = new Date().toLocaleTimeString();
  var errorPanel = document.getElementById('status_error');
  while (errorPanel.firstChild)
    errorPanel.removeChild(errorPanel.lastChild);
  if (data.error.length > 0) {
    var errorMessage = document.createElement("h3");
    errorMessage.setAttribute('class', 'error');
    errorMessage.appendChild(document.createTextNode('Error'));
    errorPanel.appendChild(errorMessage);
    for (let i=0; i<data.error.length; i++) {
      var errorElt = document.createElement("li");
      var timestamp = new Date(data.error[i].time)
      errorElt.appendChild(document.createTextNode(timestamp.toString().substr(0,24) + " " + data.error[i].msg));
      errorPanel.appendChild(errorElt);
    }
    var clearButton = document.createElement("button");
    clearButton.appendChild(document.createTextNode('Clear error messages'));
    clearButton.onclick = function(evt) {
      start_fetch();
      sendHTTP('DELETE', 'error', undefined, 'Error from server to clear error messages', function(data) {
        sendHTTP('GET', 'status', undefined, 'Error from server to update data', updateData);
      });
    };
    errorPanel.appendChild(clearButton);
  } else {
    var okMessage = document.createElement("h3");
    okMessage.setAttribute('class', 'ok');
    okMessage.appendChild(document.createTextNode('Everything is OK'));
    errorPanel.appendChild(okMessage);
  }
  stop_fetch();
}

function updateChanges() {
  var listElt = document.getElementById('mod_list');
  // clean previous content
  while (listElt.firstChild)
    listElt.removeChild(listElt.lastChild);
  // add the pending changes
  for (let paramId in pendingChanges) {
    let labelElt = document.createElement('li');
    labelElt.appendChild(document.createTextNode(paramNames[paramId].label + ": " + pendingChanges[paramId]));
    listElt.appendChild(labelElt);
  }
  // update enable/disable buttons state
  var noChanges = Object.keys(pendingChanges).length == 0;
  var buttons = document.getElementById('mod_buttonPanel').getElementsByTagName('button');
  for (let i=0; i<buttons.length; i++)
    buttons[i].disabled = noChanges;
}

function editValue(paramId, displayElt) {
  var editorElt = document.getElementById('editionPanel');
  function createButton(text, clickFunc, parent) {
    let myButton = document.createElement("button");
    myButton.appendChild(document.createTextNode(text));
    myButton.onclick = clickFunc;
    myButton.setAttribute('class', 'edit_elt edit_button');
    if (parent === undefined)
      editorElt.appendChild(myButton);
    else
      parent.appendChild(myButton);
    return myButton;
  }
  function editEnum(paramId, values) {
    for (let i=0; i<values.length; i++)
      createButton(values[i], function() {setValue(paramId, this.textContent);});
    let cancelButton = createButton("Cancel", null);
    cancelButton.setAttribute('class', 'edit_buttonEm');
    editorElt.style.display = "flex";
  }
  while (editorElt.firstChild)
    editorElt.removeChild(editorElt.lastChild);
  if (paramId == 'mode') {
    editEnum(paramId, state ? state.availableModes : []);
  } else if (paramId == 'type') {
    editEnum(paramId, state ? state.availableTypes : []);
  } else {
    let labelElt = document.createElement('span');
    labelElt.setAttribute('class', 'edit_elt');
    labelElt.appendChild(document.createTextNode(paramNames[paramId].label));
    labelElt.onclick = function(evt) {evt.stopPropagation();}
    editorElt.appendChild(labelElt);
    let inputElt = document.createElement('input');
    inputElt.setAttribute('class', 'edit_elt');
    inputElt.onclick = function(evt) {evt.stopPropagation();}
    if (paramNames[paramId].type == 'bool') {
      inputElt.setAttribute('type', 'checkbox');
      inputElt.checked = displayElt.textContent == "true";
      var okFunction = function() {setValue(paramId, inputElt.checked);};
    } else {
      inputElt.setAttribute('type', 'text');
      inputElt.value = displayElt.textContent;
      inputElt.onkeyup = function(evt) {
        if (evt.key == 'Enter') {setValue(paramId, this.value); editorElt.style.display = "none";}
      };
      var okFunction = function() {setValue(paramId, inputElt.value);};
    }
    editorElt.appendChild(inputElt);
    let buttonsDiv = document.createElement("div");
    createButton("Cancel", null, buttonsDiv);
    createButton("OK", okFunction, buttonsDiv);
    editorElt.appendChild(buttonsDiv);
    editorElt.style.display = "flex";
    inputElt.focus();
  }
}

function setValue(paramId, value) {
  if (paramNames[paramId].type == "int")
    pendingChanges[paramId] = parseInt(value);
  else if (paramNames[paramId].type == "float")
    pendingChanges[paramId] = parseFloat(value);
  else
    pendingChanges[paramId] = value;
  addClass(document.getElementById(paramNames[paramId].id), 'edited');
  updateChanges();
}

function discardChanges() {
  pendingChanges = {};
  for (var paramId in paramNames)
    removeClass(document.getElementById(paramNames[paramId].id), 'edited');
  updateChanges();
}

function commitChanges() {
  if (Object.keys(pendingChanges).length === 0)
    return;
  start_fetch();
  sendHTTP('POST', 'status', JSON.stringify(pendingChanges), 'Error from server sending new data',
           function(response) {discardChanges(); updateData(response);});
}

function saveOnFlash() {
  start_fetch();
  sendHTTP('POST', 'records', '{"action": "saveOnFlash"}', 'Error from server saving data on flash',
           function(response) {displayMessage('Data saved', 'info'); stop_fetch();});
}

function clearData() {
  var r = confirm('Deleting all history data inside brewing chamber ?');
  if (!r)
    return;
  start_fetch();
  sendHTTP('DELETE', 'records', undefined, 'Error from server clearing history data',
           function(response) {displayMessage('Data cleared', 'info'); stop_fetch();});
}

function exportData() {
  fetchData(function(data) {
    var bb = new Blob([JSON.stringify(data)], {type : 'application/json'});
    var a = document.getElementById('saveLink');
    a.href = window.URL.createObjectURL(bb);
    a.click();
    window.URL.revokeObjectURL(a.href);
  }, displayError);
}

function shutdown() {
  // go to sleep mode
  start_fetch();
  sendHTTP('POST', 'status', '{"mode": "Sleeping"}', 'Error from server requesting shutdown',
           function(response) {displayMessage('Shutdown', 'info'); updateData(response);});
}

document.addEventListener("DOMContentLoaded", function(evt) {
  // setup error report display
  var messageElt = document.getElementById('messageDisp');
  messageElt.addEventListener('transitionend', function(e) {
    if (messageElt.style.opacity == '1') {
      messageElt.style.opacity = '0';
      messageElt.style.transition = 'opacity 2s linear 5s';
    } else {
      messageElt.style.display = 'none';
    }
  });
  // add stub mark if stubed
  if (typeof stubSendHTTP != 'undefined')
    document.getElementById('stub_mark').appendChild(document.createTextNode("stubed"));
  // configure editable elements
  var allEditable = document.getElementsByClassName("editable");
  for (let i=0; i<allEditable.length; i++) {
    for (let id in paramNames) {
      if (paramNames[id].id == allEditable[i].id) {
        allEditable[i].onclick = function() {editValue(id, allEditable[i])};
        break;
      }
    }
  }
  var editorElt = document.getElementById('editionPanel');
  editorElt.onclick = function() {this.style.display = "none";};
  // create dropdown system
  var srmContentList = [];
  var dropdownElts = document.getElementsByClassName("dropdown");
  for (let i=0; i<dropdownElts.length; i++) {
    srmContentList.push(dropdownElts[i]);
    let contentElt = dropdownElts[i].getElementsByTagName('div')[0];
    contentElt.style.display = 'none';
    dropdownElts[i].getElementsByTagName('button')[0].onclick = function() {
      var contentElt = this.nextElementSibling;
      while(contentElt && contentElt.tagName != "DIV")
        contentElt = contentElt.nextElementSibling;
      if (contentElt.style.display == 'none') {
        contentElt.style.display = 'block';
      } else {
        contentElt.style.display = 'none';
        this.blur();
      }
    };
  }
  window.addEventListener("click", function(e) {
    for (let i=0; i<srmContentList.length; i++) {
      if (!srmContentList[i].contains(e.target))
        srmContentList[i].getElementsByTagName('div')[0].style.display = 'none';
    }
  });
  // create SRM dropdown content
  var srmContent = document.getElementById('srm_dropdownContent');
  for (let i=1; i<srmCodes.length+1; i++) {
    let a = document.createElement('button');
    a.onclick = function() {setSRM(this.textContent); srmContent.style.display = 'none';};
    a.appendChild(document.createTextNode(i));
    srmContent.appendChild(a);
  }
  // create menu dropdown content
  var actionsContent = document.getElementById('actions_dropdownContent');
  var buttonList = document.getElementById('nav_actions').getElementsByTagName('button');
  for (let i=0; i<buttonList.length; i++) {
    let dropButton = buttonList[i].cloneNode(true);
    dropButton.addEventListener("click", function(e) {actionsContent.style.display = 'none';});
    actionsContent.appendChild(dropButton);
  }
  // create style section for SRM display
  let styleElt = document.createElement('style');
  styleElt.id = "srmStyle";
  styleElt.setAttribute("type", "text/css");
  document.getElementsByTagName('head')[0].appendChild(styleElt);
  // set windows in initial state
  setSRM(Math.floor(Math.random() * 30) + 1);
  updateChanges(); // to align changes panel with current state
  // load current state
  start_fetch();
  sendHTTP('GET', 'status', undefined, 'Error from server to update data', updateData);
});

return {
  commitChanges: commitChanges,
  discardChanges: discardChanges,
  fetchData: fetchData,
  saveOnFlash: saveOnFlash,
  clearData: clearData,
  shutdown: shutdown,
  exportData: exportData,
  displayError: displayError
}

})();
