var stubStatus = {
  "mode": "Controlling",
  "availableModes": ["Sleeping", "Watching", "Controlling", "CoolOnly", "HeatOnly"],
  "type": "Pid",
  "availableTypes": ["Pid", "Threshold"],
  "target": 15.2,
  "temp": 8.4,
  "cooler": false,
  "heater": true,
  "startDate": "2021-07-18T08:52:12Z",
  "totalBytes": 2003582,
  "usedBytes": 130263,
  "lastSave": "2021-09-06T05:15:42Z",
  "k": 0.2,
  "R0": 10.0,
  "B": 3200.0,
  "Rref": 20.0,
  "samplePeriod": 30,
  "recordPid": true,
  "pidCycle": 600,
  "pidG": 1.0,
  "pidTi": 0.01,
  "pidTd": 0.0,
  "pidLimit": 100.0,
  "heatPeriod": 30.0,
  "heatRatio": 1.0,
  "heatPower": 0.8,
  "error": [{"time": "2021-08-18T10:23:52Z", "msg": "something went wrong"},
            {"time": "2021-08-18T10:23:57Z", "msg": "another problem occured"}]
};
var stubRecords = (function() {
  // data to send
  const files = {
    temp: [
      { fileName: 'records/20210823115218_temp',
        open: true,
        sampling: 30,
        data: [4.2, 12.4, 12.9, 25.6, 0.]
      },
      { fileName: 'records/20210823114618_temp',
        open: false,
        sampling: 30,
        data: [4.2, 12.4, 12.9, 25.6, 0.]
      }
    ],
    pid: [
      { fileName: 'records/20210823114927_pid',
        open: false,
        sampling: 10,
        data: [10.3, -2.6, 0.5e-7, 0., 0.8, 5.4, 5.5, 10.3, 15.2, -0.045]
      }
    ],
    state: [
      { fileName: 'records/20210823115226_state',
        open: true,
        door: {x: [0, 5, 98, 102, 106],
               y: [true, false, true, true, false]
        },
        cooler: {x: [0, 54, 58, 101, 104],
                 y: [false, true, false, true, false]},
        heater: {x: [0, 1, 5, 50, 52, 110],
                 y: [false, true, false, true, false, false]},
        target: {x: [0, 120], y:[12.1, 12.1]}
      }
    ]
  };

  function encodeFloat16(value) {
    var buf = new ArrayBuffer(4);
    (new Float32Array(buf))[0] = value;
    const x = (new Uint32Array(buf))[0];
    const fExp = (x>>23) & 0xff;
    if (fExp < 103) { // too small
      return (x>>16) & 0x8000; // +/- 0
    } else if (fExp < 113) { // subnormal
      return ( (x>>16) & 0x8000 ) | ( 0x400 >> (113-fExp) ) | ( (x & 0x007fffff) >> (127-fExp) );
    } else if (fExp < 143) { // normalized
      return ( (x>>16) & 0x8000 ) | ( (fExp-112) << 10 ) | ( (x>>13) & 0x03ff );
    } else if (fExp < 255) { // too big
      return ( (x>>16) & 0x8000 ) | 0x7c00; // +/- Infinity
    } else { // Infinity and NaN
      return ( (x>>16) & 0x8000 ) | 0x7c00 | ( (x>>13) & 0x03ff );
    }
  }

  // create raw data
  const doorId = 1, coolerId = 2, heaterId = 3, targetId = 0x40, spareId = 0x20;
  var stubData = {list: {temp: [], pid: [], state: [], lastSave: new Date().toString(), open: []}};
  for (let i=0; i<files.temp.length; i++) {
    let file = files.temp[i];
    let tempArr = new Uint8Array(file.data.length + 1);
    tempArr[0] = file.sampling;
    for (let j=0; j<file.data.length; j++)
      tempArr[j+1] = Math.floor((file.data[j]/30) * 255);
    stubData.list.temp.push(file.fileName);
    if (file.open)
      stubData.list.open.push(file.fileName);
    stubData[file.fileName] = tempArr.buffer;
  }
  for (let i=0; i<files.pid.length; i++) {
    let file = files.pid[i];
    let pidArr = new Uint16Array(file.data.length + 1);
    pidArr[0] = file.sampling;
    for (let j=0; j<file.data.length; j++)
      pidArr[j+1] = encodeFloat16(file.data[j]);
    stubData.list.pid.push(file.fileName);
    if (file.open)
      stubData.list.open.push(file.fileName);
    stubData[file.fileName] = pidArr.buffer;
  }
  for (let i=0; i<files.state.length; i++) {
    let file = files.state[i];
    let stateData = [];
    for (let j=0; j<file.door.x.length; j++)
      stateData.push({t: file.door.x[j], value: (file.door.y[j] ? 1 : 0), id: doorId});
    for (let j=0; j<file.cooler.x.length; j++)
      stateData.push({t: file.cooler.x[j], value: (file.cooler.y[j] ? 1 : 0), id: coolerId});
    for (let j=0; j<file.heater.x.length; j++)
      stateData.push({t: file.heater.x[j], value: (file.heater.y[j] ? 1 : 0), id: heaterId});
    for (let j=0; j<file.target.x.length; j++)
      stateData.push({t: file.target.x[j], value: file.target.y[j], id: targetId});
    stateData.sort(function(x, y){return x.t - y.t;});
    let stateArr = new Uint32Array(stateData.length);
    for (let j=0; j<stateData.length; j++) {
      if (stateData[j].id > 0x1f) {
        if (stateData[j].id == targetId) {
          stateArr[j] = (stateData[j].t << 10) + (0x200)
                        + (Math.floor((stateData[j].value/30) * 255) & 0xff);
        }else {
          stateArr[j] = (stateData[j].t << 10) + (0x300) + (stateData[j].value & 0xff);
        }
      } else {
        stateArr[j] = (stateData[j].t << 10) + ((stateData[j].id & 0x1f) << 4)
                      + (stateData[j].value & 0xf);
      }
    }
    stubData.list.state.push(file.fileName);
    if (file.open)
      stubData.list.open.push(file.fileName);
    stubData[file.fileName] = stateArr.buffer;
  }
  stubData.list = JSON.stringify(stubData.list);
  return stubData;
})();

stubSendHTTP = function(method, url, content, errorMsg, successFunc, errorFunc) {
  setTimeout(function() {
    if (url == 'status') {
      if (method == 'POST') {
        const changes = JSON.parse(content);
        for (let paramId in changes) {
          stubStatus[paramId] = changes[paramId];
        }
      }
      successFunc(stubStatus);
    } else if (url == 'error') {
      if (method == 'DELETE')
        stubStatus.error = [];
      successFunc(stubStatus.error);
    } else if (url == 'records') {
      if (method == 'DELETE') {
        stubRecords = {list: JSON.stringify({temp: [], pid: [], state: [], lastSave: new Date().toString(), open: []})};
      } else if (method == 'POST') {
        var stubData = JSON.parse(stubRecords.list);
        stubData.lastSave = new Date().toString();
        stubRecords.list = JSON.stringify(stubData);
      }
      successFunc(JSON.parse(stubRecords.list));
    } else if (url.substring(0, 'records/'.length) == 'records/') {
      if (method == 'DELETE') {
        delete stubRecords[url];
        var stubData = JSON.parse(stubRecords.list);
        stubData.temp = stubData.temp.filter(function(item) {return item !== url});
        stubData.pid = stubData.pid.filter(function(item) {return item !== url});
        stubData.state = stubData.state.filter(function(item) {return item !== url});
        stubData.open = stubData.open.filter(function(item) {return item !== url});
        stubRecords.list = JSON.stringify(stubData);
      }
      successFunc(stubRecords[url]);
    }
  }, 500);
};
