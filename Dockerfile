FROM debian:bullseye

# system utilities
ENV PATH=/arduino/bin:$PATH
RUN apt-get update && apt-get install -y curl python3 python3-pip \
  && mkdir -p /arduino/bin \
# arduino-cli
  && curl -fsSL https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh \
  | BINDIR=/arduino/bin sh -s 0.20.2 \
# littlefs tools
  && curl -fsSL https://github.com/earlephilhower/mklittlefs/releases/download/3.0.0/x86_64-linux-gnu-mklittlefs-295fe9b.tar.gz \
  | tar -xzC /arduino/bin --strip-components 1 \
  && pip3 install esptool \
# esp8266 core software
  && arduino-cli core update-index --additional-urls https://arduino.esp8266.com/stable/package_esp8266com_index.json \
  && arduino-cli core install esp8266:esp8266 --additional-urls https://arduino.esp8266.com/stable/package_esp8266com_index.json \
# additional arduino libraries
  && arduino-cli lib install NTPClient ArduinoJson Time
