# Home brewing temperature management


## hardware used

+ fridge, used as isolated enclosure and cooler
+ heating resistance
+ temperature sensor (thermistor)
+ home made control board based on ESP8266

### control board

The main element is a esp-12f board based on ESP8266 that connect to wifi to monitor the regulator.
The board also include a voltage regulator to provide 3.3V to the microcontroller,
two relay drivers and an amplifier to adapt thermistor measure of temperature.

![board schematic](conception/circuit_wifi_board.png)

### data recorded

On the control page some history data can be displayed, the data itself is stored in the board flash memory.
Without PID values it can hold almost a year of continuous record.

The data format is described in this [document](conception/boardInterface.md) with HTTP interface.


## Build and flash

Before using it change address and wifi password in code, or give it as compilation parameters.

The build and flash operation use *arduino-cli* inside docker container.

Build docker image and start the container
```bash
# build docker image
louis@oryx:~/brew-drive$ docker build . -t arduino-cli-esp8266
# run the container
louis@oryx:~/brew-drive$ docker run -it --rm -v $PWD:/project -w /project --name brew-drive arduino-cli-esp8266 bash
```

Build the projects files to upload to board
```bash
# compile software
root@9a2190adf3a2:/project# arduino-cli compile -e -b esp8266:esp8266:generic board_src
# compile software with custom wifi information
root@9a2190adf3a2:/project# arduino-cli compile -e -b esp8266:esp8266:generic --build-property "compiler.cpp.extra_flags=\"-DWIFI_SSID=\"customSSID\"\" \"-DWIFI_PASS=\"customSecret\"\"" board_src
# compile software with debug messages
root@9a2190adf3a2:/project# arduino-cli compile -e -b esp8266:esp8266:generic --build-property compiler.cpp.extra_flags=-DDEBUG_MSG board_src
# create the zipped web page
root@93006840903e:/project# ./makeESPhtml.py
# create littlefs image
root@9a2190adf3a2:/project# mklittlefs -c board_src/data -p 256 -b 8192 -s 2072576 board_src/build/board_src.mklittlefs.bin
```

Flash the board, the port may have to be changed
```bash
# once the board is connected check on which port, should be /dev/ttyUSB* or /dev/ttyACM*
louis@oryx:~/brew-drive$ ls /dev/tty*
# run the container with access to board serial port
louis@oryx:~/brew-drive$ docker run -it --rm -v $PWD:/project -w /project --device=/dev/ttyUSB0:/dev/ttyUSB0 --name brew-drive arduino-cli-esp8266 bash
# flash soft
root@93006840903e:/project# arduino-cli upload -b esp8266:esp8266:generic -p /dev/ttyUSB0 -t -i board_src/build/esp8266.esp8266.generic/board_src.ino.bin
# flash littlefs image
root@9a2190adf3a2:/project# esptool.py --chip esp8266 --port /dev/ttyUSB0 --baud 115200 write_flash --flash_size detect 0x200000 board_src/build/board_src.mklittlefs.bin
```

## Work in progress

### C code on the ESP8266

+ calibrate the temperature sensor

### main page

+ create a section to manage files inside the board, to delete old files
+ make auto-refresh work
+ improve the parameters display

### for the *chartLine* element

+ add a button to show/hide each curve, with auto zoom
+ print a lighter secondary grid without values
+ display the curve values on hover
