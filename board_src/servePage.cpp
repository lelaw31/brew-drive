#include "servePage.h"
#include "coreHelper.h"
#include <LittleFS.h>

ESP8266WebServer server = ESP8266WebServer(80);

void handleNotFound() {
  if (server.uri().startsWith("/records/") && LittleFS.exists(server.uri())) {
    handleRecords();
    return;
  }
#ifdef DEBUG_MSG
  Serial.print(F("Requested not found resource: "));
  Serial.println(server.uri());
#endif
  server.send(404, F("text/plain"), F("File Not Found\n\n"));
}

void handleError() {
  if (server.method() != HTTP_GET && server.method() != HTTP_HEAD && server.method() != HTTP_DELETE) {
    server.send(405, F("text/plain"), F("only method GET, HEAD and DELETE are supported for this url\n\n"));
    return;
  }
  if (server.method() == HTTP_DELETE) {
#ifdef DEBUG_MSG
    Serial.println(F("clean error messages request"));
#endif
    bcs.clearErrors();
  }
  String message;
  if (server.method() != HTTP_HEAD) {
    StaticJsonDocument<512> doc;
    bcs.addJsonErrorArray(&doc);
    serializeJson(doc, message);
  }
#ifdef DEBUG_MSG
  Serial.println(F("error request successfully processed"));
#endif
  server.send(200, F("application/json"), message);
}

void handleRoot() {
  if (server.method() != HTTP_GET && server.method() != HTTP_HEAD) {
    server.send(405, F("text/plain"), F("only method GET, HEAD and PUT are supported for this url\n\n"));
    return;
  }
  if (server.method() == HTTP_PUT) {
    File f = LittleFS.open(F("/index.html.gz"), "w");
    if (!f) {
      bcs.recordError("Opening index file for writing failed");
      server.send(500, F("text/plain"), F("Opening gz index file for writing failed\n\n"));
      return;
    }
    f.print(server.arg("plain"));
    f.close();
    server.send(201, F("text/plain"), F("Index file successfully updated.\n\n"));
#ifdef DEBUG_MSG
    Serial.println(F("Index file successfully updated"));
#endif
  } else {
    File f = LittleFS.open(F("/index.html.gz"), "r");
    if (!f) {
      bcs.recordError("Opening index file failed");
      server.send(500, F("text/plain"), F("Opening gz index file failed\n\n"));
      return;
    }
    server.setContentLength(f.size());
    server.sendHeader(F("Content-Encoding"), F("gzip"));
    server.send(200, F("text/html"), "");
    if (server.method() != HTTP_HEAD)
      f.sendAll(server.client());
    f.close();
#ifdef DEBUG_MSG
    Serial.println(F("Sent index as gz encoded file"));
#endif
  }
}

void handleStatus() {
  if (server.method() != HTTP_GET && server.method() != HTTP_HEAD && server.method() != HTTP_POST) {
    server.send(405, F("text/plain"), F("only method GET, HEAD and POST are supported for this url\n\n"));
    return;
  }
  String message;
  if (server.method() == HTTP_POST) {
#ifdef DEBUG_MSG
    Serial.println(F("request parameter update"));
#endif
    DynamicJsonDocument reqDoc(1024);
    DeserializationError error = deserializeJson(reqDoc, server.arg("plain").c_str());
    if (error) {
      server.send(500, F("text/plain"), "deserializeJson() on request body failed for : '"
                                        + server.uri() + String("': ") + error.f_str() + "\n\n");
      return;
    }
    bcs.setParamFromJson(reqDoc, false);
    message = bcs.createParametersJson();
    File paramFile = LittleFS.open("/param.json", "w");
    paramFile.print(message);
    paramFile.close();
  } else if (server.method() != HTTP_HEAD) {
    message = bcs.createParametersJson();
  }
#ifdef DEBUG_MSG
  Serial.println(F("status request successfully processed"));
#endif
  server.send(200, F("application/json"), message);
}

void handleRecordList() {
  if (server.method() != HTTP_GET && server.method() != HTTP_HEAD
      && server.method() != HTTP_POST && server.method() != HTTP_DELETE) {
    server.send(405, F("text/plain"), F("only method GET, HEAD, POST and DELETE are supported for this url\n\n"));
    return;
  }
  if (server.method() == HTTP_DELETE) {
    if (!history.clean()) {
      server.send(500, F("text/plain"), F("Removing data files failed\n\n"));
      return;
    }
  } else if (server.method() == HTTP_POST) {
    StaticJsonDocument<512> reqDoc;
    DeserializationError error = deserializeJson(reqDoc, server.arg("plain").c_str());
    if (error) {
      server.send(500, F("text/plain"), "deserializeJson() on request body failed for : '"
                                        + server.uri() + "': " + error.f_str() + "\n\n");
      return;
    }
    if (!reqDoc.containsKey(F("action"))) {
      server.send(418, F("text/plain"), "Unexpected POST request content at '" + server.uri() + "'\n\n");
      return;
    }
    String reqAction = reqDoc[F("action")];
#ifdef DEBUG_MSG
    Serial.print(F("request record action: "));
    Serial.println(reqAction);
#endif
    if (reqAction == F("saveOnFlash")) {
      history.flush(false);
    } else  {
      server.send(418, F("text/plain"), "Unknown POST action requested at '" + server.uri()
                                        + "' :" + reqAction + "\n\n");
      return;
    }
  }
  String message;
  if (server.method() != HTTP_HEAD) {
    DynamicJsonDocument doc(2048);
    doc["lastSave"] = NTPService::jsonDate(history.getlastWrite());
    JsonArray tempArray = doc.createNestedArray("temp");
    JsonArray pidArray = doc.createNestedArray("pid");
    JsonArray stateArray = doc.createNestedArray("state");
    Dir records = LittleFS.openDir("/records");
    while (records.next()) {
      String fileName = records.fileName();
      if (fileName.endsWith(F("_temp")))
        tempArray.add("records/" + fileName);
      else if (fileName.endsWith(F("_pid")))
        pidArray.add("records/" + fileName);
      else if (fileName.endsWith(F("_state")))
        stateArray.add("records/" + fileName);
    }
    JsonArray openArray = doc.createNestedArray("open");
    String openFile = history.getCurrentFileNameStart();
    if (!openFile.isEmpty()) {
      openArray.add("records/" + openFile + "_temp");
      if (history.getRecordPid())
        openArray.add("records/" + openFile + "_pid");
      openArray.add("records/" + openFile + "_state");
    }
    serializeJson(doc, message);
  }
#ifdef DEBUG_MSG
  Serial.print(F("send record list: "));
  Serial.println(message);
#endif
  server.send(200, F("application/json"), message);
}

void handleRecords() {
  if (server.method() != HTTP_GET && server.method() != HTTP_HEAD && server.method() != HTTP_DELETE) {
    server.send(405, F("text/plain"), F("only method GET, HEAD and DELETE are supported for this url\n\n"));
    return;
  }
  history.handleRecordRequest(server);
}
