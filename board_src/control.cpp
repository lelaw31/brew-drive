#include "control.h"
#define LOW 0
#define HIGH 1

Regulation regulator;

const char *Regulation::availableModes[] = {"Sleeping", "Watching", "Controlling", "CoolOnly", "HeatOnly"};
const char *Regulation::availableTypes[] = {"Pid", "Threshold"};

Regulation::Regulation()
  : _mode(Watching), _type(Threshold), _activeMode(0.f), _changeMode(0.f), _lastRelayUsed(None),
    _cycleDuration_ms(600000ul), _heatPowerRatio(1), _lastControl(0), _cycleStart(0)
{}

void Regulation::setThresholdParams(float activeMode, float changeMode)
{
  _activeMode = activeMode;
  _changeMode = changeMode;
}

void Regulation::setPidParams(float g, float ti, float td, float limit)
{
  _pid.setParams(g, ti, td, limit);
}

void Regulation::setPidCycleDuration(int duration_s)
{
  _cycleDuration_ms = ((unsigned long) duration_s) * 1000ul;
}

void Regulation::setHeatPowerRatio(float ratio)
{
  _heatPowerRatio = ratio;
}

bool Regulation::setMode(ControlMode mode, float temp, unsigned long time_ms)
{
  if (_mode == mode)
    return false;
  _mode = mode;
  init(temp, time_ms);
  return true;
}

bool Regulation::setType(ControlType type, float temp, unsigned long time_ms)
{
  if (_type == type)
    return false;
  _type = type;
  init(temp, time_ms);
  return true;
}

void Regulation::setTarget(float target) {_pid.setOrder(target);}

Regulation::ControlMode Regulation::mode() const {return _mode;}
Regulation::ControlType Regulation::type() const {return _type;}
float Regulation::target() const {return _pid.order();}
const PidRegulator &Regulation::pid() const {return _pid;};
int Regulation::pidCycleDuration() const {return _cycleDuration_ms / 1000;}
float Regulation::heatPowerRatio() const {return _heatPowerRatio;}

void Regulation::init(float temp, unsigned long time_ms)
{
  _pid.init(temp);
  _lastRelayUsed = None;
  _lastControl = time_ms;
  _cycleStart = time_ms;
}

bool Regulation::control(float temp, unsigned long time_ms, Action *action, int *lenght_s, float *pidValue)
{
  *action = NoAction;
  *lenght_s = 0;
  if (_mode == Watching || _mode == Sleeping)
    return true;

  if (_type == Pid)
    return pidControl(temp, time_ms, action, lenght_s, pidValue);
  else if (_type == Threshold)
    return thresholdControl(temp, action, lenght_s);
  else
    return false;
}

bool Regulation::thresholdControl(float temp, Action *action, int *lenght_s)
{
  float delta = temp - _pid.order();
  *lenght_s = -1;

  if (delta < -_changeMode) {
    if (_mode == Controlling || _mode == HeatOnly) {
      *action = Heat;
      _lastRelayUsed = Heater;
    }

  } else if (delta < -_activeMode) {
    if ( (_mode == Controlling || _mode == HeatOnly) && _lastRelayUsed != Cooler) {
      *action = Heat;
      _lastRelayUsed = Heater;
    }

  } else if (delta > _changeMode) {
    if (_mode == Controlling || _mode == CoolOnly) {
      *action = Cool;
      _lastRelayUsed = Cooler;
    }

  } else if (delta > _activeMode) {
    if ( (_mode == Controlling || _mode == CoolOnly) && _lastRelayUsed != Heater) {
      *action = Cool;
      _lastRelayUsed = Cooler;
    }
  }

  return true;
}

bool Regulation::pidControl(float temp, unsigned long time_ms, Action *action, int *lenght_s, float *pidValue)
{
  float dt_s = (float) (time_ms - _lastControl) / 1000.f;
  *pidValue = _pid.control(temp, dt_s);
  if (time_ms - _cycleStart > _cycleDuration_ms) {
    _cycleStart = time_ms;
    if (*pidValue > 0) {
      *action = Cool;
      *lenght_s = *pidValue;
    } else {
      *action = Heat;
      *lenght_s = *pidValue / _heatPowerRatio;
    }
  }
  _lastControl = time_ms;
  return true;
}

PidRegulator::PidRegulator() : PidRegulator(1.f, 0.f, 0.f, 1000.f) {}

PidRegulator::PidRegulator(float g, float ti, float td, float iLimit)
  : _g(g), _ti(ti), _td(td), _iLimit(iLimit), _order(12.f), _i(0.f), _error(0.f)
{}

void PidRegulator::setParams(float g, float ti, float td, float iLimit)
{
  _g = g;
  _ti = ti;
  _td = td;
  _iLimit = iLimit;
  if (_i > _iLimit)
    _i = _iLimit;
  else if (_i < -_iLimit)
    _i = -_iLimit;
}

float PidRegulator::order() const {return _order;}
void PidRegulator::setOrder(float order) {_order = order;}
float PidRegulator::g() const {return _g;}
float PidRegulator::ti() const {return _ti;}
float PidRegulator::td() const {return _td;}
float PidRegulator::limit() const {return _iLimit;}

void PidRegulator::init(float measure) {
  _i = 0.f;
  _error = _order - measure;
}

float PidRegulator::control(float measure, float dt_s) {
  float error = (_order - measure);
  float d = 0.f;
  if (dt_s > 1e-9) {
    _i += error * dt_s;
    if (_i > _iLimit)
      _i = _iLimit;
    else if (_i < -_iLimit)
      _i = -_iLimit;
    d = (error - _error) / dt_s;
    _error = error;
  }
  return _g * ( error + _i * _ti + d * _td);
}
