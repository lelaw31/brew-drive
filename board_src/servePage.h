#include <ESP8266WebServer.h>

extern ESP8266WebServer server;

void handleNotFound();
void handleRoot();
void handleStatus();
void handleError();
void handleRecordList();
void handleRecords();
