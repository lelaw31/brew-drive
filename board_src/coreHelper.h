#include <ArduinoJson.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <ESP8266WebServer.h>
#include "control.h"

#define MAX_ERROR_STATUS 5
#define DATA_BUFFER_SIZE 1440 // minutes in one day
#define FILE_SIZE_LIMIT 45000 // a bit more than the minutes in a month, a bit less than 11 * 4ko

struct ChamberStatus {
  static const uint8_t relayCold;
  static const uint8_t relayHot;
  static const uint8_t doorSensor;

  ChamberStatus();

  // system helpers
  void recordError(const String &message);
  void clearErrors();
  bool timeForAction();

  // parameters
  void setControlMode(Regulation::ControlMode mode);
  void setSamplePeriod(int value);
  void setTarget(float target);
  void setHeaterParameters(float heatPeriod, float heatRatio, float heatPower);
  int samplePeriod() const;

  // states
  void readStartTime();
  void updateTemperature();
  void updateRelays(Regulation::Action action, int lenght_s);
  void recordDoorState(bool state);
  float temperature() const;
  bool temperatureOk() const;
  bool hotRelay() const;
  bool coldRelay() const;
  bool doorOpen() const;
  char sampleStep() const;

  //parameter formating
  JsonVariant getParameter(JsonDocument &doc, const char *name);
  void setParamFromJson(JsonDocument &doc, bool withDefault);
  void readParameterFile();
  void addJsonErrorArray(JsonDocument *doc);
  String createParametersJson();

private:
  struct LogLine {
    LogLine();
    LogLine(time_t time, const String &message);
    time_t time;
    String message;
  };
  // parameters
  float _k;
  float _R0;
  float _beta;
  float _Rref;
  int _samplePeriod; // second
  float _heatPeriod;
  float _heatRatio;
  float _heatPower;
  // system states
  time_t _startTime;
  float _temperature;
  bool _hotRelay;
  bool _coldRelay;
  Regulation::Action _requestAction;
  time_t _requestEnd;
  bool _doorOpen;
  time_t _nextControl;
  char _sampleStep; // the number of samplePeriod passed since last step, should be always 1
  unsigned long _pwmActionStart;
  // error recording
  LogLine _error[MAX_ERROR_STATUS];
  char _errorStatusIdx;
  char _tempError; // avoid floading the same error every temperature reading

  float readTemperature();
  void setRelayState(uint8_t relay, uint8_t value);
};

class NTPService {
public:
  static String jsonDate(time_t t);

  NTPService();

  time_t getTime();
  void begin();

private:
  WiFiUDP ntpUDP;
  NTPClient timeClient;
};

class HistoryData {
public:
  enum BooleanId {Door=1, Cooler=2, Heater=3};

  void addValues(float temp, float pid);
  void addBoolean(bool value, BooleanId id);
  void addTempTarget(float target);
  time_t getlastWrite() const;
  void flush(bool forceCloseFile);
  bool clean();
  void handleRecordRequest(ESP8266WebServer &server);
  String getCurrentFileNameStart() const;
  bool getRecordPid() const;
  void setRecordPid(bool record);

private:
  unsigned char tempBuffer[DATA_BUFFER_SIZE];
  unsigned int pidBuffer[DATA_BUFFER_SIZE];
  unsigned long stateBuffer[DATA_BUFFER_SIZE];
  bool recordPid = false;
  int valuesIdx = 0;
  int stateIdx = 0;
  time_t currentFileStart = 0;
  time_t lastWrite = 0;

  void startNewFile(unsigned char temp, unsigned int pid);
  int stateClosingSize() const;
  int addStateClosing(unsigned long buffer[], int idx, time_t t) const;
  void addValuesToBuffer(unsigned char tempChar, unsigned int pidInt);
};

extern ChamberStatus bcs;
extern NTPService timeService;
extern HistoryData history;
