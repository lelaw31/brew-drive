
/* command(p) = G * (1 + 1/(Ti p) + Td p) */
class PidRegulator {
public:
  PidRegulator();
  PidRegulator(float g, float ti, float td, float iLimit);

  void setParams(float g, float ti, float td, float iLimit);
  void setOrder(float order);
  void init(float measure);
  float control(float measure, float dt_s);

  float order() const;
  float g() const;
  float ti() const;
  float td() const;
  float limit() const;

private:
  // params
  float _g;
  float _ti;
  float _td;
  float _iLimit;
  // order
  float _order;
  // internal states
  float _i;
  float _error;
};

class Regulation {
public:
  enum ControlMode {Sleeping=0, Watching=1, Controlling=2, CoolOnly=3, HeatOnly=4};
  enum ControlType {Pid, Threshold};
  enum Action {NoAction, Heat, Cool};

  static const char *availableModes[5];
  static const char *availableTypes[2];

  Regulation();

  void setThresholdParams(float activeMode, float changeMode);
  void setPidParams(float g, float ti, float td, float limit);
  void setPidCycleDuration(int duration_s);
  void setHeatPowerRatio(float ratio);
  bool setMode(ControlMode mode, float temp, unsigned long time_ms);
  bool setType(ControlType type, float temp, unsigned long time_ms);
  void setTarget(float target);

  ControlMode mode() const;
  ControlType type() const;
  float target() const;
  const PidRegulator &pid() const;
  int pidCycleDuration() const;
  float heatPowerRatio() const;

  void init(float temp, unsigned long time_ms);
  bool control(float temp, unsigned long time_ms, Action *action, int *lenght_s, float *pidValue);

private:
  enum Relay {None, Heater, Cooler};
  bool thresholdControl(float temp, Action *action, int *lenght_s);
  bool pidControl(float temp, unsigned long time_ms, Action *action, int *lenght_s, float *pidValue);

  ControlMode _mode;
  ControlType _type;
  // threshold control parameters
  float _activeMode;
  float _changeMode;
  Relay _lastRelayUsed;
  // pid control parameters
  PidRegulator _pid;
  unsigned long _cycleDuration_ms;
  float _heatPowerRatio;
  unsigned long _lastControl;
  unsigned long _cycleStart;
};

extern Regulation regulator;
