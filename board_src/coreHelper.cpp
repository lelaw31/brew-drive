#include "coreHelper.h"
#include <LittleFS.h>
#include <TimeLib.h>

NTPService timeService;
ChamberStatus bcs;
HistoryData history;

/************************
*      NTPService       *
************************/
String NTPService::jsonDate(time_t t) {
  int z = month(t);
  String monthStr = z < 10 ? "0" + String(z) : String(z);
  z = day(t);
  String dayStr = z < 10 ? "0" + String(z) : String(z);
  z = hour(t);
  String hourStr = z < 10 ? "0" + String(z) : String(z);
  z = minute(t);
  String minuteStr = z < 10 ? "0" + String(z) : String(z);
  z = second(t);
  String secondStr = z < 10 ? "0" + String(z) : String(z);
  return String(year(t)) + "-" + monthStr + "-" + dayStr + "T" + hourStr + ":" + minuteStr + ":" + secondStr + "Z";
}

NTPService::NTPService() : ntpUDP(), timeClient(ntpUDP, "europe.pool.ntp.org", 0, 120000) {}

void NTPService::begin() {
  timeClient.begin();
}

time_t NTPService::getTime() {
  timeClient.update();
  return timeClient.getEpochTime();
}

/************************
*     ChamberStatus     *
************************/
const uint8_t ChamberStatus::relayCold = 4;
const uint8_t ChamberStatus::relayHot = 5;
const uint8_t ChamberStatus::doorSensor = 16;

ChamberStatus::LogLine::LogLine() : time(0) {}
ChamberStatus::LogLine::LogLine(time_t time, const String &message) : time(time), message(message) {}

ChamberStatus::ChamberStatus() {
  _k = 0.f;
  _R0 = 0.f;
  _beta = 0.f;
  _Rref = 0.f;
  _samplePeriod = 60;
  _heatPeriod = 30.f;
  _heatRatio = 1.f;
  _heatPower = 0.5f;
  _pwmActionStart = 0l;
  _startTime = 0;
  _temperature = -500.f;
  _hotRelay = false;
  _coldRelay = false;
  _doorOpen = false;
  _requestAction = Regulation::NoAction;
  _requestEnd = 0;
  _nextControl = 0;
  _sampleStep = 0;
  _errorStatusIdx = 0;
  _tempError = 0;
}

void ChamberStatus::recordError(const String &message) {
  Serial.print(F("[ERROR] "));
  Serial.println(message);
  _error[_errorStatusIdx] =  LogLine(timeService.getTime(), message);
  _errorStatusIdx = (_errorStatusIdx+1) % MAX_ERROR_STATUS;
}

void ChamberStatus::clearErrors() {
  for (int i=0; i<MAX_ERROR_STATUS; i++) {
    _error[i].time = 0;
    _error[i].message.clear();
  }
  _errorStatusIdx = 0;
}

int ChamberStatus::samplePeriod() const { return _samplePeriod; }
float ChamberStatus::temperature() const { return _temperature; }
bool ChamberStatus::hotRelay() const { return _hotRelay; }
bool ChamberStatus::coldRelay() const { return _coldRelay; }
bool ChamberStatus::doorOpen() const { return _doorOpen; }
char ChamberStatus::sampleStep() const { return _sampleStep; }

void ChamberStatus::readStartTime() {
  _startTime = timeService.getTime();
  _nextControl = _startTime;
}

bool ChamberStatus::temperatureOk() const {
  return _temperature > -275. && _temperature < 400.;
}

void ChamberStatus::updateTemperature() {
  bool tempOk = temperatureOk();
  _temperature = readTemperature();
  if (!tempOk && temperatureOk())
    regulator.init(_temperature, millis());
}

void ChamberStatus::setControlMode(Regulation::ControlMode mode) {
  if (!regulator.setMode(mode, _temperature, millis()))
    return;
  Serial.print(F("Set control mode to "));
  Serial.println(Regulation::availableModes[mode]);
  updateRelays(Regulation::NoAction, 0);
  if (mode == Regulation::Sleeping)
    history.flush(true);
}

void ChamberStatus::setSamplePeriod(int value) {
  if (_samplePeriod == value)
    return;
  Serial.print(F("Set sample period to "));
  Serial.println(value);
  _samplePeriod = value;
  history.flush(true);
}

void ChamberStatus::setTarget(float target) {
  regulator.setTarget(target);
  Serial.print(F("Set target temperature to "));
  Serial.println(target);
  history.addTempTarget(target);
}

void ChamberStatus::setHeaterParameters(float heatPeriod, float heatRatio, float heatPower)
{
  _heatPeriod = heatPeriod;
  _heatRatio = heatRatio;
  _heatPower = heatPower;
  regulator.setHeatPowerRatio(_heatPower * _heatRatio);
}

void ChamberStatus::setRelayState(uint8_t relay, uint8_t value) {
  if (relay == relayCold)
    _coldRelay = value == HIGH;
  else
    _hotRelay = value == HIGH;
  digitalWrite(relay, value);
}

void ChamberStatus::updateRelays(Regulation::Action action, int lenght_s) {
  if (!bcs.temperatureOk()) {
    setRelayState(relayCold, LOW);
    setRelayState(relayHot, LOW);
    return;
  }
  if (action == Regulation::NoAction) {
    if (_requestAction == Regulation::NoAction)
      return;
    if (_requestEnd > 0 && _requestEnd < timeService.getTime()) {
      setRelayState(relayCold, LOW);
      setRelayState(relayHot, LOW);
      _requestAction = Regulation::NoAction;
    } else if (_requestAction = Regulation::Heat) {
      unsigned long posInCycle = millis() - _pwmActionStart;
      if (posInCycle > _heatPeriod * 1000) {
        _pwmActionStart = millis();
        setRelayState(relayHot, HIGH);
      } else if (posInCycle > _heatRatio * _heatPeriod * 1000) {
        setRelayState(relayHot, LOW);
      }
    }
  } else {
    _requestAction = action;
    if (lenght_s < 0)
      _requestEnd = 0;
    else
      _requestEnd = timeService.getTime() + lenght_s;
    _pwmActionStart = millis();
#ifdef DEBUG_MSG
    Serial.print(F("Activate relay "));
    Serial.println(action);
#endif
    setRelayState(relayCold, action == Regulation::Cool ? HIGH : LOW);
    setRelayState(relayHot, action == Regulation::Heat ? HIGH : LOW);
  }
}

void ChamberStatus::recordDoorState(bool state) {
  if (_doorOpen == state)
    return;
  _doorOpen = state;
  if (regulator.mode() != Regulation::Sleeping)
    history.addBoolean(state, HistoryData::Door);
}

bool ChamberStatus::timeForAction() {
  time_t currentTime = timeService.getTime();
  if (currentTime < _nextControl)
    return false;
  _sampleStep = 1;
  _nextControl = _nextControl + _samplePeriod;
  while (_nextControl < currentTime) {
    _nextControl = _nextControl + _samplePeriod;
    _sampleStep++;
  }
  if (_sampleStep > 1)
    recordError("Control loop overflow, passed several steps at once : " + (int)_sampleStep);
  return true;
}

float ChamberStatus::readTemperature() {
  int reading = analogRead(A0);
  // check read value
  if (reading < 20) {
    if (_tempError != 10) {
      recordError("Temperature too high to measure");
      _tempError = 10;
    }
    return 500.f;
  }
  if (reading > 1020) {
    if (_tempError != 11) {
      recordError("Temperature too low to measure");
      _tempError = 11;
    }
    return -500.f;
  }
  int resistance = _k * reading + _R0;
  if (abs(resistance - _Rref) < 0.01) {
    if (_tempError != 1) {
      recordError("Error computing temperature, parameters may be wrong");
      _tempError = 1;
    }
    return -500.f;
  }
  float temp = _beta / log(resistance/_Rref) - 273.15;
#ifdef DEBUG_MSG
  Serial.print(F("Read temperature: "));
  Serial.println(temp);
#endif
  if ( temp < -3 || temp > 40) {
    if (_tempError != 2) {
      recordError("Out of bound temperature, parameters may be wrong");
      _tempError = 2;
    }
    return -500.f;
  }
  _tempError = 0;
  return temp;
}

JsonVariant ChamberStatus::getParameter(JsonDocument &doc, const char *name) {
  if (!doc.containsKey(name))
    return JsonVariant();
  // check value
  JsonVariant value = doc[name];
  bool valOk = true;
  if (!strcmp(name, "Rref")) {
    valOk = value > 0.001;
  } else if (!strcmp(name, "mode")) {
    valOk = false;
    for (int i=0; i<sizeof(Regulation::availableModes)/sizeof(char*); i++) {
      if (value == Regulation::availableModes[i]) {
        valOk = true;
        break;
      }
    }
  } else if (!strcmp(name, "type")) {
    valOk = false;
    for (int i=0; i<sizeof(Regulation::availableTypes)/sizeof(char*); i++) {
      if (value == Regulation::availableTypes[i]) {
        valOk = true;
        break;
      }
    }
  } else if (!strcmp(name, "target")) {
    valOk = 4 <= value && value <= 25;
  }
  if (!valOk) {
    recordError("Wrong value for parameter " + String(name));
    return JsonVariant();
  }
  return value;
}

void ChamberStatus::setParamFromJson(JsonDocument &doc, bool withDefault) {
  JsonVariant value = getParameter(doc, "mode");
  if (!value.isNull()) {
    for (int i=0; i<sizeof(Regulation::availableModes)/sizeof(char*); i++) {
      if (value == Regulation::availableModes[i]) {
        setControlMode((Regulation::ControlMode) i);
        break;
      }
    }
  } else if (withDefault)
    setControlMode(Regulation::Watching);

  value = getParameter(doc, "type");
  if (!value.isNull()) {
    for (int i=0; i<sizeof(Regulation::availableTypes)/sizeof(char*); i++) {
      if (value == Regulation::availableTypes[i]) {
        regulator.setType((Regulation::ControlType) i, _temperature, millis());
        break;
      }
    }
  } else if (withDefault)
    regulator.setType(Regulation::Threshold, _temperature, millis());

  value = getParameter(doc, "target");
  if (!value.isNull())
    setTarget(value);
  else if (withDefault)
    setTarget(18.f);

  value = getParameter(doc, "k");
  if (!value.isNull())
    _k = value;
  else if (withDefault)
    _k = 50.f;

  value = getParameter(doc, "R0");
  if (!value.isNull())
    _R0 = value;
  else if (withDefault)
    _R0 = 10000.f;

  value = getParameter(doc, "B");
  if (!value.isNull())
    _beta = value;
  else if (withDefault)
    _beta = 2000.f;

  value = getParameter(doc, "Rref");
  if (!value.isNull())
    _Rref = value;
  else if (withDefault)
    _Rref = 200.f;

  value = getParameter(doc, "samplePeriod");
  if (!value.isNull())
    setSamplePeriod(value);
  else if (withDefault)
    setSamplePeriod(30);

  value = getParameter(doc, "recordPid");
  if (!value.isNull())
    history.setRecordPid(value);
  else if (withDefault)
    history.setRecordPid(false);

  // heater parameters
  float heatPeriod = _heatPeriod;
  float heatRatio = _heatRatio;
  float heatPower = _heatPower;
  if (withDefault) {
    heatPeriod = 30.f;
    heatRatio = 1.f;
    heatPower = 0.5f;
  }
  value = getParameter(doc, "heatPeriod");
  if (!value.isNull())
    heatPeriod = value;
  value = getParameter(doc, "heatRatio");
  if (!value.isNull())
    heatRatio = value;
  value = getParameter(doc, "heatPower");
  if (!value.isNull())
    heatPower = value;
  setHeaterParameters(heatPeriod, heatRatio, heatPower);

  // PID relulator parameters
  float pidG = regulator.pid().g();
  float pidTi = regulator.pid().ti();
  float pidTd = regulator.pid().td();
  float pidLimit = regulator.pid().limit();
  if (withDefault) {
    pidG = 1.f;
    pidTi = 0.01f;
    pidTd = 0.f;
    pidLimit = 100.f;
  }
  value = getParameter(doc, "pidG");
  if (!value.isNull())
    pidG = value;
  value = getParameter(doc, "pidTi");
  if (!value.isNull())
    pidTi = value;
  value = getParameter(doc, "pidTd");
  if (!value.isNull())
    pidTd = value;
  value = getParameter(doc, "pidLimit");
  if (!value.isNull())
    pidLimit = value;
  regulator.setPidParams(pidG, pidTi, pidTd, pidLimit);

  value = getParameter(doc, "pidCycle");
  if (!value.isNull())
    regulator.setPidCycleDuration(value);
  else if (withDefault)
    regulator.setPidCycleDuration(600);
}

void ChamberStatus::readParameterFile() {
  File paramFile = LittleFS.open("/param.json", "r");
  if(!paramFile) {
    recordError("opening parameter file failed");
    return;
  }
  DynamicJsonDocument paramJson(1024);
  DeserializationError error = deserializeJson(paramJson, paramFile);
  paramFile.close();
  if (error) {
    recordError(String("deserializeJson() on parameter file failed: ") + error.f_str());
    return;
  }
  setParamFromJson(paramJson, true);
  Serial.println(F("Parameter file loaded"));
}

void ChamberStatus::addJsonErrorArray(JsonDocument *doc) {
  JsonArray msgArray = doc->createNestedArray("error");
  for (int i=0; i<MAX_ERROR_STATUS; i++) {
    const LogLine &err = _error[(i+_errorStatusIdx)%MAX_ERROR_STATUS];
    if (!err.message.isEmpty()) {
      JsonObject errorLine = msgArray.createNestedObject();
      errorLine["time"] = NTPService::jsonDate(err.time);
      errorLine["msg"] = err.message;
    }
  }
}

String ChamberStatus::createParametersJson() {
  DynamicJsonDocument paramJson(2048);
  paramJson["mode"] = Regulation::availableModes[regulator.mode()];
  JsonArray modeArray = paramJson.createNestedArray("availableModes");
  for (int i=0; i<sizeof(Regulation::availableModes)/sizeof(char*); i++)
    modeArray.add(Regulation::availableModes[i]);
  paramJson["type"] = Regulation::availableTypes[regulator.type()];
  JsonArray typeArray = paramJson.createNestedArray("availableTypes");
  for (int i=0; i<sizeof(Regulation::availableTypes)/sizeof(char*); i++)
    typeArray.add(Regulation::availableTypes[i]);
  paramJson["target"] = regulator.target();
  paramJson["temp"] = _temperature;
  paramJson["cooler"] = _coldRelay;
  paramJson["heater"] = _hotRelay;
  paramJson["startDate"] = NTPService::jsonDate(_startTime);
  FSInfo fs_info;
  LittleFS.info(fs_info);
  paramJson["totalBytes"] = fs_info.totalBytes;
  paramJson["usedBytes"] = fs_info.usedBytes;
  paramJson["lastSave"] = NTPService::jsonDate(history.getlastWrite());
  paramJson["k"] = _k;
  paramJson["R0"] = _R0;
  paramJson["B"] = _beta;
  paramJson["Rref"] = _Rref;
  paramJson["samplePeriod"] = _samplePeriod;
  paramJson["recordPid"] = history.getRecordPid();
  paramJson["pidG"] = regulator.pid().g();
  paramJson["pidTi"] = regulator.pid().ti();
  paramJson["pid_td"] = regulator.pid().td();
  paramJson["pidLimit"] = regulator.pid().limit();
  paramJson["pidCycle"] = regulator.pidCycleDuration();
  paramJson["heatPeriod"] = _heatPeriod;
  paramJson["heatRatio"] = _heatRatio;
  paramJson["heatPower"] = _heatPower;
  addJsonErrorArray(&paramJson);
  String buffer;
  serializeJson(paramJson, buffer);
  return buffer;
}

/************************
*      HistoryData      *
************************/
static unsigned char encodeTemperature(float temp) {
  if (temp < 0.f)
    return 0;
  if (temp > 30.f)
    return 255;
  return (temp/30.f) * 255.f;
}
// inspired by http://www.fox-toolkit.org/ftp/fasthalffloatconversion.pdf
static unsigned int encodePidValue(float pid) {
  unsigned long x = *((unsigned long*) &pid);
  unsigned char fExp = (x>>23) & 0xff;
  if (fExp < 103) { // too small
    return (x>>16) & 0x8000; // +/- 0
  } else if (fExp < 113) { // subnormal
    return ( (x>>16) & 0x8000 ) | ( 0x400 >> (113-fExp) ) | ( (x & 0x007fffff) >> (127-fExp) );
  } else if (fExp < 143) { // normalized
    return ( (x>>16) & 0x8000 ) | ( (fExp-112) << 10 ) | ( (x>>13) & 0x03ff );
  } else if (fExp < 255) { // too big
    return ( (x>>16) & 0x8000 ) | 0x7c00; // +/- Infinity
  } else { // Infinity and NaN
    return ( (x>>16) & 0x8000 ) | 0x7c00 | ( (x>>13) & 0x03ff );
  }
}

// these two function must be changed together
// addStateClosing() should increase idx by stateClosingSize()
int HistoryData::stateClosingSize() const { return 4;}
int HistoryData::addStateClosing(unsigned long buffer[], int idx, time_t t) const {
  // record end of file states
  unsigned long timeStamp = (t - currentFileStart) << 10;
  buffer[idx++] = timeStamp + (((char) Door & 0x1f) << 4) + (bcs.doorOpen() ? 1 : 0);
  buffer[idx++] = timeStamp + (((char) Heater & 0x1f) << 4) + (bcs.hotRelay() ? 1 : 0);
  buffer[idx++] = timeStamp + (((char) Cooler & 0x1f) << 4) + (bcs.coldRelay() ? 1 : 0);
  buffer[idx++] = timeStamp + 0x200 + encodeTemperature(regulator.target());
  return idx; // must be argument 'idx' + stateClosingSize()
}

void HistoryData::addValues(float temp, float pid) {
  unsigned char tempChar = encodeTemperature(temp);
  unsigned int pidInt = recordPid ? encodePidValue(pid) : 0;
  if (currentFileStart == 0) {
    startNewFile(tempChar, pidInt);
    return;
  }
  // if some steps are missing, interpolate their data
  if (bcs.sampleStep() > 1) {
    unsigned char tempRef = valuesIdx > 0 ? tempBuffer[valuesIdx-1] : tempChar;
    float tempStep = ((float)(tempRef-tempChar)) / (float)bcs.sampleStep();
    unsigned int pidRef = valuesIdx > 0 ? pidBuffer[valuesIdx-1] : pidInt;
    float pidStep = ((float)(pidRef-pidInt)) / (float)bcs.sampleStep();
    for (int i=0; i<bcs.sampleStep()-1; i++)
      addValuesToBuffer(tempRef + tempStep*i, pidRef + pidStep*i);
  }
  addValuesToBuffer(tempChar, pidInt);
  // check if it is time to flush
  if (timeService.getTime() - currentFileStart > 86400) // 24 * 60 * 60 => one day
    flush(false);
}

void HistoryData::addValuesToBuffer(unsigned char tempChar, unsigned int pidInt) {
  if (recordPid)
    pidBuffer[valuesIdx] = pidInt;
  tempBuffer[valuesIdx++] = tempChar;
  if (valuesIdx == DATA_BUFFER_SIZE)
    flush(false);
}

void HistoryData::addBoolean(bool value, BooleanId id) {
  // don't start new file for boolean just record them with a timestamp at 0
  // the file need to be created by the temperature addition for its time to be acurate
  unsigned int timeStamp = (currentFileStart == 0) ? 0 : (timeService.getTime() - currentFileStart);
  stateBuffer[stateIdx++] = (timeStamp << 10) + (((char) id & 0x1f) << 4) + (value ? 1 : 0);
  // - stateClosingSize() to keep room for file closing data
  if (stateIdx == DATA_BUFFER_SIZE - stateClosingSize())
    flush(false);
}

void HistoryData::addTempTarget(float target) {
  unsigned int timeStamp = (currentFileStart == 0) ? 0 : (timeService.getTime() - currentFileStart);
  stateBuffer[stateIdx++] = (timeStamp << 10) + 0x200 + encodeTemperature(target);
  // - stateClosingSize() to keep room for file closing data
  if (stateIdx == DATA_BUFFER_SIZE - stateClosingSize())
    flush(false);
}

void HistoryData::setRecordPid(bool record) {
  if (recordPid == record)
    return;
  recordPid = record;
  flush(true);
}

bool HistoryData::getRecordPid() const {return recordPid;}
time_t HistoryData::getlastWrite() const {return lastWrite;}

String HistoryData::getCurrentFileNameStart() const {
  if (currentFileStart == 0)
    return String();
  int z = month(currentFileStart);
  String monthStr = z < 10 ? "0" + String(z) : String(z);
  z = day(currentFileStart);
  String dayStr = z < 10 ? "0" + String(z) : String(z);
  z = hour(currentFileStart);
  String hourStr = z < 10 ? "0" + String(z) : String(z);
  z = minute(currentFileStart);
  String minuteStr = z < 10 ? "0" + String(z) : String(z);
  z = second(currentFileStart);
  String secondStr = z < 10 ? "0" + String(z) : String(z);
  return "/records/" + String(year(currentFileStart)) + monthStr + dayStr + hourStr + minuteStr + secondStr;
}

void HistoryData::flush(bool forceCloseFile) {
  if (currentFileStart == 0) {
    valuesIdx = 0;
    stateIdx = 0;
    return;
  }
  // check for closing files
  time_t t = timeService.getTime();
  bool closeFiles = forceCloseFile || month(currentFileStart) != month(t);
  // get fileName
  String fileNameStart = getCurrentFileNameStart();
  // flush temperature file
  File tempFile = LittleFS.open(fileNameStart + "_temp", "a");
  if(!tempFile) {
    bcs.recordError("opening temperature file for write failed: " + fileNameStart + "_temp");
  } else {
    tempFile.write(tempBuffer, valuesIdx);
    closeFiles = closeFiles || tempFile.size() > FILE_SIZE_LIMIT;
    tempFile.close();
  }
  // flush pid file
  if (recordPid) {
    File pidFile = LittleFS.open(fileNameStart + "_pid", "a");
    if(!pidFile) {
      bcs.recordError("opening pid file for write failed: " + fileNameStart + "_pid");
    } else {
      pidFile.write((const unsigned char *)pidBuffer, valuesIdx*sizeof(unsigned int));
      closeFiles = closeFiles || pidFile.size() > FILE_SIZE_LIMIT;
      pidFile.close();
    }
  }
  valuesIdx = 0;

  // flush state file
  File stateFile = LittleFS.open(fileNameStart + "_state", "a");
  if(!stateFile) {
    bcs.recordError("opening state file for write failed: " + fileNameStart + "_state");
  } else {
    closeFiles = closeFiles || tempFile.size() + (stateIdx+stateClosingSize())*sizeof(unsigned long) > FILE_SIZE_LIMIT;
    if (closeFiles)
      stateIdx = addStateClosing(stateBuffer, stateIdx, t); // record end of file states
    stateFile.write((const unsigned char *)stateBuffer, stateIdx*sizeof(unsigned long));
    stateFile.close();
  }
  stateIdx = 0;

  // update file state tracking
  lastWrite = t;
#ifdef DEBUG_MSG
  Serial.print(F("File flushed: "));
  Serial.println(fileNameStart);
#endif
  if (closeFiles)
    currentFileStart = 0;

  // check free space size
  FSInfo fs_info;
  LittleFS.info(fs_info);
  size_t bytesLeft = fs_info.totalBytes - fs_info.usedBytes;
  if (bytesLeft < FILE_SIZE_LIMIT)
    bcs.recordError("File system near or completely full, only " + String(bytesLeft) + " bytes left");
}

static bool startFile(const String &fileName, uint8_t *samplePeriod, const char *value, int valueSize) {
  File file = LittleFS.open(fileName, "w");
  if(!file) {
    bcs.recordError("Opening file for init failed: " + fileName);
    return false;
  }
  if (samplePeriod != 0)
    file.write(valueSize == 1 ? *samplePeriod : (uint16_t) *samplePeriod);
  if (valueSize > 0)
    file.write(value, valueSize);
  file.close();
  return true;
}

void HistoryData::startNewFile(unsigned char temp, unsigned int pid) {
  if (currentFileStart != 0)
    flush(true);
  currentFileStart = timeService.getTime();
  // get fileName
  String fileNameStart = getCurrentFileNameStart();
  uint8_t samplePeriod = (unsigned char) bcs.samplePeriod();
  // init temperature file
  if (!startFile(fileNameStart + "_temp", &samplePeriod, (const char *) &temp, sizeof(unsigned char))) {
    currentFileStart = 0;
    return;
  }
  // init pid file
  if (recordPid)
    startFile(fileNameStart + "_pid", &samplePeriod, (const char *) &pid, sizeof(unsigned int));
  // init state file
  if(startFile(fileNameStart + "_state", 0, (const char *) stateBuffer, stateIdx*sizeof(unsigned long)))
    stateIdx = 0;
  // keep date of this write operation
  lastWrite = currentFileStart;
  Serial.print(F("File started: '"));
  Serial.print(fileNameStart);
  Serial.print(F("', with sample period: "));
  Serial.println((int)samplePeriod);

}

bool HistoryData::clean() {
  currentFileStart = 0;
  stateIdx = 0;
  valuesIdx = 0;
  String allRecords[50];
  char recordIdx = 0;
  bool res = true;
  Dir records = LittleFS.openDir("/records");
  while (records.next()) {
    allRecords[recordIdx++] = records.fileName();
    if (recordIdx == 50) {
      for (int i=0; i<recordIdx; i++) {
        if (!LittleFS.remove("/records/" + allRecords[i]))
          res = false;
      }
      recordIdx = 0;
    }
  }
  for (int i=0; i<recordIdx; i++) {
    if (!LittleFS.remove("/records/" + allRecords[i]))
      res = false;
  }
  if (!res)
    bcs.recordError("Removing all records failed");
  return res;
}

void HistoryData::handleRecordRequest(ESP8266WebServer &server) {
  const String &ressource = server.uri();
  bool isOpenFile = ressource.startsWith(getCurrentFileNameStart());
  // handle DELETE method and return
  if (server.method() == HTTP_DELETE) {
    if (!LittleFS.remove(ressource)) {
      bcs.recordError("Deleting record failed: " + ressource);
      server.send(500, "text/plain", "Deleting record failed\n\n");
      return;
    }
#ifdef DEBUG_MSG
    Serial.print(F("Deleted record "));
    Serial.println(ressource);
#endif
    if (isOpenFile) {
      currentFileStart = 0;
      stateIdx = 0;
      valuesIdx = 0;
    }
    server.send(200, "application/octet-stream", "");
    return;
  }
  // open file and prepare header
  File resourceFile = LittleFS.open(ressource, "r");
  if (!resourceFile) {
    bcs.recordError("Opening record failed: " + ressource);
    server.send(500, "text/plain", "Opening record failed\n\n");
    return;
  }
  bool isTempFile = ressource.endsWith("_temp");
  bool isPidFile = ressource.endsWith("_pid");
  int fileSize = resourceFile.size();
  int bufferSize = 0;
  if (isOpenFile) {
    if (isTempFile)
      bufferSize = valuesIdx * sizeof(unsigned char);
    else if (isPidFile)
      bufferSize = valuesIdx * sizeof(unsigned int);
    else
      bufferSize = (stateIdx + stateClosingSize()) * sizeof(unsigned long);
  }
  server.setContentLength(fileSize + bufferSize);
  server.send(200, "application/octet-stream", "");
  // if method is HEAD don't send message body
  if (server.method() == HTTP_HEAD) {
#ifdef DEBUG_MSG
    Serial.print(F("send HTTP_HEAD record "));
    Serial.println(ressource);
#endif
    return;
  }
#ifdef DEBUG_MSG
  Serial.print(F("send record "));
  Serial.println(ressource);
#endif
  // send file content
  ssize_t sent = resourceFile.sendAll(server.client());
  if (sent != fileSize)
    bcs.recordError(String("Sending record file, short send after timeout (")+sent+"<"+fileSize+")");
  resourceFile.close();
  // if file is open send buffered data
  if (isOpenFile) {
    if (isTempFile) {
      StreamConstPtr ref(tempBuffer, valuesIdx);
      sent = ref.sendSize(&server.client(), valuesIdx);
    } else if (isPidFile) {
      StreamConstPtr ref((char *)pidBuffer, valuesIdx*sizeof(unsigned int));
      sent = ref.sendSize(&server.client(), valuesIdx*sizeof(unsigned int));
    } else {
      // send buffer
      StreamConstPtr ref((char *)stateBuffer, stateIdx*sizeof(unsigned long));
      sent = ref.sendSize(&server.client(), stateIdx*sizeof(unsigned long));
      // add current state
      int currentStateSize = stateClosingSize() * sizeof(unsigned long);
      unsigned long *values = (unsigned long *) malloc(currentStateSize);
      int stateSize = addStateClosing(values, 0, timeService.getTime());
      StreamConstPtr ref2((char *)values, currentStateSize);
      sent += ref2.sendSize(&server.client(), currentStateSize);
      free(values);
    }
    if (sent != bufferSize)
      bcs.recordError(String("Sending record buffer, short send after timeout (")+sent+"<"+bufferSize+")");
  }
}
