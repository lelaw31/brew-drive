#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <LittleFS.h>
#include "coreHelper.h"
#include "servePage.h"

/* TODO:
 *  - finish test on board
 *  - fix change target temperature
 *  - improve regulation algo
 *  - put regulation parameters in configuration
*/

#ifndef WIFI_SSID
#define WIFI_SSID "MySSID"
#endif
#ifndef WIFI_PASS
#define WIFI_PASS "SomeSecret"
#endif

// wifi credential parameters
const char *ssid = WIFI_SSID;
const char *password = WIFI_PASS;

void setup(void) {
  // set pin state
  pinMode(ChamberStatus::relayCold, OUTPUT);
  digitalWrite(ChamberStatus::relayCold, 0);
  pinMode(ChamberStatus::relayHot, OUTPUT);
  digitalWrite(ChamberStatus::relayHot, 0);
  pinMode(ChamberStatus::doorSensor, INPUT);

  // start connection
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  // get time
  timeService.begin();

  // Launch LittleFS file system
  if (!LittleFS.begin()) {
    bcs.recordError("An Error has occurred while mounting LittleFS");
  } else {
    Serial.println("File system is mounted");
  }
  if (!LittleFS.exists("/records")) {
    if (LittleFS.mkdir("/records"))
      Serial.println(F("directory '/records' successfully created"));
    else
      bcs.recordError("Creating directory '/records' failed");
  }

  // HTTP server configuration
  server.onNotFound(handleNotFound);
  server.on("/", handleRoot);
  server.on("/index.html", handleRoot);
  server.on("/status", handleStatus);
  server.on("/error", handleError);
  server.on("/records", handleRecordList);
  // "/records/*" is routed through not found

  // read operating parameters
  bcs.readParameterFile();

  // init regulator and start service
  bcs.readStartTime();
  bcs.updateTemperature();
  server.begin();
  Serial.println("HTTP server started");
}

void loop(void) {
  server.handleClient();
  bcs.recordDoorState(digitalRead(ChamberStatus::doorSensor) == HIGH);
  if (bcs.timeForAction()) {
    Regulation::Action action = Regulation::NoAction;
    int lenght_s = 0;
    float pidValue = 0.f;
    bcs.updateTemperature();
    if (bcs.temperatureOk()) {
      if (!regulator.control(bcs.temperature(), millis(), &action, &lenght_s, &pidValue))
        bcs.recordError("computing relay state for control");
#ifdef DEBUG_MSG
      Serial.print(F("Pid value: "));
      Serial.println(pidValue);
#endif
    }
    bcs.updateRelays(action, lenght_s);
    if (regulator.mode() != Regulation::Sleeping)
      history.addValues(bcs.temperature(), pidValue);
  } else {
    bcs.updateRelays(Regulation::NoAction, 0);
  }
}
